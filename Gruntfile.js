module.exports = function(grunt) {
    "use strict";
    grunt.initConfig({
        compare_size: {
            files: ["dist/js/bootstrap.js", "dist/js/bootstrap.min.js"],
            options: {
                compress: {
                    gz: function(contents) {
                        return require("gzip-js").zip(contents, {}).length;
                    }
                },
                cache: "build/.sizecache.json"
            }
        },
        merge_data: {
            ru: {
                src: ["i18n/ru/*.json"],
                dest: "dist/lang/ru.json"
            },
            en: {
                src: ["i18n/en/*.json"],
                dest: "dist/lang/en.json"
            },
            all: {
                src: ["i18n/*/*.json"],
                dest: "dist/lang/all.json"
            }

        },
        lmd: {
            dev: {
                projectRoot: './',
                build: 'developer',
                options: {
                    output: 'dist/js/bootstrap.js',
                    log: true,
                    warn: true
                }
            }
        },
//        copy: {
//            "dest": {
//                src: 'dist/*',
//                dest: '/var/www'
//            }
//        },
        size: {
            dev: {
                src: ['dist/js/*.js']
            }
        },
        shell: {
            dev: {
                command: 'cp -rf dist /var/www'
            }
        },
        clean:{
              dist: ["dist"]
        },
        watch: {
            configFiles: {
                files: ['Gruntfile.js'],
                options: {
                    reload: true
                }
            },
            lang: {
                files: 'i18n/*/*.json',
                tasks: ['merge_data'],
                options: {
                    livereload: true
                }
            }
        }

    });
    grunt.loadNpmTasks('grunt-lmd');
    grunt.loadNpmTasks('grunt-merge-data');
    grunt.loadNpmTasks('grunt-size');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.registerTask("default", ["clean:dist","merge_data", "lmd:dev", "shell:dev", "size:dev"]);
};
