function fullContent() {
    var session = kernel.session;
    if (session.is('fullContent') === undefined) {
        session.set('fullContent',false);
    }
    if (session.get('fullContent') === true) {
        document.getElementById("content").children[0].style.display = "";
        document.getElementById("content").children[1].style.padding = "";
        document.getElementById("content").children[2].style.display = "";
        document.getElementById("lefta").style.display = "block";
        document.getElementById("centera").style.left = "220px";
        document.getElementById("centera").style.right = "220px";
        document.getElementById("righta").style.display = "block";
        document.getElementById("big").style.display = "none";
        session.set('fullContent',false);
    } else {
        document.getElementById("content").children[0].style.display = "none";
        document.getElementById("content").children[1].style.padding = "0";
        document.getElementById("content").children[2].style.display = "none";
        document.getElementById("lefta").style.display = "none";
        document.getElementById("centera").style.left = "0";
        document.getElementById("centera").style.right = "0";
        document.getElementById("righta").style.display = "none";
        document.getElementById("big").style.display = "inline-block";
        session.set('fullContent',true);
    }
}
function loading(mode){
    if(mode === undefined){
        mode = (document.getElementById("circularG").style.display === 'none' )?true:false;
    }
    if(mode){
        document.getElementById("circularG").style.display = "block";
        document.getElementById("circular").style.display = "block";
    }else{
        document.getElementById("circularG").style.display = "none";
        document.getElementById("circular").style.display = "none";
    }
    
}
module.exports = {
    fullContent: fullContent,
    loading:loading
};