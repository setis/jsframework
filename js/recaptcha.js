/*
 var cfg = {
 id: 'recaptcha',
 key: null,
 options: {
 lang: 'ru',
 theme: 'clean',
 callback: Recaptcha.focus_response_field
 },
 send: function() {
 return {
 cmd: "recaptcha",
 data: {
 key: this.key,
 challenge: Recaptcha.get_challenge(),
 response: Recaptcha.get_response()
 }
 };
 }
 };
 */
function recaptcha(cfg, Recaptcha) {
    var errors = new require('translation').lang(kernel.translates, 'recaptcha');
    Recaptcha = Recaptcha || window.Recaptcha;
    var recaptcha = {
        key: null,
        id: null,
        on: {},
        options: {},
        constructor: function(cfg) {
            if (cfg !== undefined) {
                if (cfg.id !== undefined) {
                    this.id = cfg.id;
                }
                if (cfg.key !== undefined) {
                    this.key = cfg.key;
                }
                if (cfg.options !== undefined && cfg.options instanceof Object) {
                    this.options = cfg.options;
                }
                if (cfg.on !== undefined && cfg.on instanceof Object) {
                    var on = cfg.on, list = ['Success', 'Error'], self = this;
                    list.forEach(function(name) {
                        var func = on[name];
                        if (func !== undefined && typeof func === "function") {
                            self.on[name] = function() {
                                return func.apply(self, arguments);
                            };
                        }
                    });
                }
            }
            this.create();
            return this;
        },
        create: function() {
            Recaptcha.create(this.key, this.id, this.options);
            setTimeout(function(){
                if(document.getElementById('recaptcha_challenge_image') === null){
//                    console.info('fixet:recaptcha');
                    Recaptcha.reload();
                }
            },750);
            return this;
        },
        success: function() {
            var status;
            if (this.on.Success !== undefined && this.on.Success() === false) {
                status = this.on.Success();
            }else{
                Recaptcha.destroy();
            }
            return this;
        },
        error: function(e) {
            Recaptcha.reload();
            console.info(e);
            if (this.on.Error !== undefined) {
                this.on.Error(errors.translate(e));
            }
            return this;
        },
        document: function() {
            return document.getElementById(this.id);
        }

    };
    recaptcha.self = Recaptcha;
    return recaptcha.constructor(cfg);
}
module.exports = recaptcha;