function Link(cfg) {
    function stop(e) {
        if (!e) {
            window.event.cancelBubble = true;
        } else {
            e.stopPropagation();
            e.preventDefault();
        }
    }
    var link = {
        hrefs: function(html, event, func) {
            if (html === undefined) {
                return false;
            }
            var a = html.getElementsByTagName("a");
            for (var i = 0; i < a.length; i++) {

//                var event = a[i].onclick = function() {
//                    func(this);
//                    return false;
//                };

                a[i].addEventListener(event, function(e) {
                    if (func(this) === false) {
                        stop(e);
                    }else{
                        e.preventDefault();
                    }
                }, true);
            }
        },
        handlers: {},
        handler: function(html, event, func, name, onwrite) {
//            console.info('link.handler '+name,this.handlers[name] === undefined, this.handlers[name] === false,onwrite === true);
            if (this.handlers[name] === undefined || this.handlers[name] === false || onwrite === true) {
                this.hrefs(html, event, func);
                this.handlers[name] = true;
                return true;
            }
            return false;
        },
        cfg: {},
        load: function(links) {
            if (links === undefined && typeof links === 'object') {
                return false;
            }
            for (var name in links) {
                var obj = links[name];
                if (obj !== undefined && typeof obj.document === 'function' && typeof obj.onwrite === 'boolean') {
                    this.cfg[name] = obj;
                } else {
                    console.info('not link.load ' + name, obj);
                }
            }
            return true;
        },
        unload: function(name) {
            if (name === undefined) {
                this.cfg = {};
                return this;
            }
            switch (typeof name) {
                case 'string':
                    if (this.cfg[name] !== undefined) {
                        delete this.cfg[name];
                    }
                    break;
                case 'object':
                    for (var i = 0; i <= name.length; i++) {
                        var index = name[i];
                        if (this.cfg[index] !== undefined) {
                            delete this.cfg[index];
                        }

                    }
            }
            return this;
        },
        loaded: function() {
            var result = [];
            for (var name in this.cfg) {
                result.push(name);
            }
            return result;
        },
        one: function(name, onwrite) {
            if (this.cfg[name] === undefined) {
                return false;
            }
            var obj = this.cfg[name];
            var write = obj.onwrite;
            if (onwrite !== undefined && typeof onwrite === 'boolean') {
                write = onwrite;
            }
            this.handler(obj.document(), obj.event, obj.handler, name, write);
            return this;
        },
        all: function(onwrite) {
            for (var name in this.cfg) {
                var obj = this.cfg[name];
                if (obj !== undefined) {
                    var write = obj.onwrite;
                    if (onwrite !== undefined && typeof onwrite === 'boolean') {
                        write = onwrite;
                    }
                    var status = this.handler(obj.document(), obj.event, obj.handler, name, write);
                    // console.info('link.all ' + name, status);
                }
            }
            return this;
        },
        list: function(list, onwrite) {
            for (var i = 0; i <= list.length; i++) {
                var name = list[i];
                if (this.cfg[name] !== undefined) {
                    var obj = this.cfg[name];
                    var write = obj.onwrite;
                    if (onwrite !== undefined && typeof onwrite === 'boolean') {
                        write = onwrite;
                    }
                    this.handler(obj.document(), obj.event, obj.handler, name, write);
                }

            }
            return this;
        },
        constructor: function(cfg) {
            if (cfg !== undefined && typeof cfg === 'object') {
                this.load(cfg);
            }
            return this;
        }

    };
    return link.constructor(cfg);

}
module.exports = Link;

//var links = {
//    footer: {
//        document: function() {
//            return document.getElementById('footer');
//        },
//        event: 'click',
//        handler: function(id, callback) {
//            history.pushState('', '', id.getAttribute('href'));
//            core.event.exec('click');
//            if (typeof callback === 'function') {
//                callback();
//            }
//            return false;
//        },
//        onwrite: false
//    }
//};
//var link = new Link(links);