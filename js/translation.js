function translation(translates, code) {
    var lang = {
        code: null,
        translates: {},
        constructor: function(translates, code) {
            if (code === undefined) {
                this.code = window.navigator.language;
            } else {
                this.code = code;
            }
            if (translates !== undefined) {
                this.translates = translates;
            }
            return this;
        },
        translate: function(id) {
            if (id === undefined) {
                return false;
            }
            if (typeof id === 'string') {
                var arr = id.split('.');
            }
            if (arr.length > 1) {
                var translate = this.translates[this.code];
                for (var i in arr) {
                    translate = translate[arr[i]];
                    if (translate === undefined) {
                        console.info('translate: ' + this.code + ': '.arr[i]);
                        return false;
                    }
                }
                return translate;
            }
            if (this.translates[this.code][id] === undefined) {
                console.info('translate: ' + this.code + ': '.id);
                return false;
            }
            return this.translates[this.code][id];

        }
    };
    return lang.constructor(translates, code);
}
function plugin(translates, use) {
    var pluginLang = {
        use_translate: null,
        translates: {},
        translate: function(id) {
            if (id === undefined) {
                return false;
            }
            if (typeof id === 'string') {
                var arr = id.split('.');
            }
            if (arr.length > 1) {
                for (var i in arr) {
                    var translate = this.translates[arr[i]];
                    if (translate === undefined) {
                        console.info('translate: ' + arr[i]);
                        return false;
                    }
                }
                return translate;
            }
            if (this.translates[id] === undefined) {
                console.info('translate: ' + id);
                return false;
            }
            return this.translates[id];

        },
        constructor: function(translates, use) {
            if (use !== undefined && translates !== undefined) {
                var select = require('functions').select, self = this;
//                console.info(translates,use);
                select(translates, use, function(use, select) {
//                    console.info(use,select);
                    self.translates = use;
                    self.use_translate = select;
                }, function() {
                    console.info("not select pluginLang: " + use.toString(), translates);
                });
            } else if (translates !== undefined) {
                this.translates = translates;
            }
            return this;
        }

    };
    return pluginLang.constructor(translates, use);
}
module.exports = {
    lang:translation,
    plugin:plugin
};