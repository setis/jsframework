var check = new require('functions').check();
function login(cfg) {
    var login = {
        block: true,
        on: {},
        events: {},
        access: false,
        bind: function() {
            var self = this,list = ['login', 'password'];
            for (var i in list) {
                var name = list[i];
                this.events[name] = this.on[name]().addEventListener('keyup', function() {
                    self[name]();
                    self.check();
                }, false);
            }
            this.on.recaptcha().addEventListener('DOMNodeInserted', function(e) {
                e.target.addEventListener('keyup', function() {
                    self.recaptcha();
                    self.check();
                }, false);
            }, false);
            this.events['lock'] = this.on.lock().querySelector('div').addEventListener('click', function() {
                self.lock();
                self.check();
            }, false);
            this.events['submit'] = this.on.submit().addEventListener('click', function() {
                if (self.access === true) {
                    self.send();
                }
            }, false);

        },
        unbind: function() {
            this.events.forEach(function(event) {
                document.removeEventListener(event);
            });
        },
        login: function() {
            var id = this.on.login();
            if (id.value === "") {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не обходимо ввести </span></div>";
                return false;
            }
            if (id.value.length < this.cfg.login.min) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не менее " + this.cfg.login.min + " символов</span></div>";
                return false;
            } else if (id.value.length > this.cfg.login.max) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не более " + this.cfg.login.max + " символов</span></div>";
                return false;
            } else {
                if (check.mail(id.value) === false) {
                    id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Некорректный E-mail</span></div>";
                    return false;
                } else {
                    id.parentNode.nextSibling.innerHTML = "<div class='rred sgreen'><div><div></div></div><span>Всё отлично</span></div>";
                    return true;
                }
            }
        },
        exec: function() {
            if (this.check() === false) {
                return false;
            }
            this.send();
        },
        lock: function() {
            if (this.block === true) {
                this.on.password().setAttribute("type", "text");
                this.on.lock().style.background = "url(/img/icon/offpassico.png) no-repeat";
                this.block = false;
            } else {
                this.on.password().setAttribute("type", "password");
                this.on.lock().style.background = "url(/img/icon/passico.png) no-repeat";
                this.block = true;
                
            }
            this.check();
        },
        check: function() {
            if (this.login() && this.password() && this.recaptcha()) {
                this.on.submit().className = "btmon";
                this.access = true;
                return true;
            } else {
                this.on.submit().className = "btmoff";
                this.access = false;
                return false;
            }
        },
        password: function() {
            var id = this.on.password();
            if (id.value === "") {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не обходимо ввести</span></div>";
                return false;
            }
            if (id.value.length < this.cfg.password.min) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не менее " + this.cfg.password.min + " символов</span></div>";
                return false;
            }
            id.parentNode.nextSibling.innerHTML = "";
            return true;
        },
        recaptcha: function() {
            var id = Recaptcha.get_response(), is = this.on.recaptcha().nextElementSibling;
            if (id === "") {
                return false;
            }
            if (id.length < 3) {
                is.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не указан код подтверждения</span></div>";
                return false;
            } else {
                is.innerHTML = "";
                return true;
            }
        },
        send: function() {
        },
        cfg: {},
        constructor: function(cfg) {
            if (cfg !== undefined && typeof cfg === 'object') {
                if (cfg.on !== undefined) {
                    this.on = cfg.on;
                }
                if (cfg.send !== undefined && typeof cfg.send === 'function') {
                    this.send = cfg.send;
                }
                if (cfg.login !== undefined) {
                    this.cfg.login = cfg.login;
                }
                if (cfg.password !== undefined) {
                    this.cfg.password = cfg.password;
                }
                if (cfg.lock !== undefined && typeof cfg.lock === 'boolean') {
                    this.block = cfg.lock;
                }
                this.cfg = cfg;
            }
            this.bind();
            return this;
        }
    };
    return login.constructor(cfg);
}
function register(cfg) {
    var register = {
        block: true,
        on: {},
        events: {},
        access: false,
        bind: function() {
            var self = this, list = ['login', 'password', 'recaptcha', 'repeatPassword'];
            for (var i in list) {
                var name = list[i];
                this.events[name] = this.on[name]().addEventListener('keyup', function() {
                    self[name]();
                    self.check();
                }, false);
            }
            this.on.recaptcha().addEventListener('DOMNodeInserted', function(e) {
                e.target.addEventListener('keyup', function() {
                    self.recaptcha();
                    self.check();
                }, false);
            }, false);
            this.events['lock'] = this.on.lock().querySelector('div').addEventListener('click', function() {
                self.lock();
                self.check();
            }, false);
            this.events['contract'] = this.on.contract()[0].addEventListener('change', function() {
                self.contract();
                self.check();
            }, false);
            this.events['submit'] = this.on.submit().addEventListener('click', function() {
                if (self.access === true) {
                    self.send();
                }
            }, false);


        },
        unbind: function() {
            this.events.forEach(function(event) {
                document.removeEventListener(event);
            });
        },
        exec: function() {
            if (this.check() === false) {
                return false;
            }
            this.send();
        },
        check: function() {
            if (this.login() && this.password() && this.repeatPassword() && this.recaptcha() && this.contract()) {
                this.on.submit().className = "btmon";
                this.access = true;
                return true;
            } else {
                this.on.submit().className = "btmoff";
                this.access = false;
                return false;
            }
        },
        login: function() {
            var id = this.on.login();
            if (id.value === "") {
                return false;
            }
            if (id.value.length < this.cfg.login.min) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не менее " + this.cfg.login.min + " символов</span></div>";
                return false;
            } else if (id.value.length > this.cfg.login.max) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не более " + this.cfg.login.max + " символов</span></div>";
                return false;
            } else {
                if (check.mail(id.value) === false) {
                    id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Некорректный E-mail</span></div>";
                    return false;
                } else {
                    id.parentNode.nextSibling.innerHTML = "<div class='rred sgreen'><div><div></div></div><span>Всё отлично</span></div>";
                    return true;
                }
            }
        },
        password: function() {
            var id = this.on.password();
            if (id.value === "") {
                return false;
            }
            if (id.value.length < this.cfg.password.min) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не менее " + this.cfg.password.min + " символов</span></div>";
                return false;
            }
            if (check.levelPassword(id.value) === 1) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Очень слабый</span></div>";
            } else if (check.levelPassword(id.value) === 2) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Слабый</span></div>";
            } else if (check.levelPassword(id.value) === 3) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred syellow'><div><div></div></div><span>Средний</span></div>";
            } else if (check.levelPassword(id.value) === 4) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sgreen'><div><div></div></div><span>Сильный</span></div>";
            }
            return true;
        },
        repeatPassword: function() {
            var id = this.on.repeatPassword();
            if (this.block === false) {
                return true;
            }
            if (id.value === "") {
                return false;
            }
            var rpass = this.on.password().value;
            if (id.value.length < this.cfg.password.min) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не менее " + this.cfg.password.min + " символов</span></div>";
                return false;
            }
            if (rpass === id.value && id.value !== "") {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sgreen'><div><div></div></div><span>Отлично</span></div>";
                return true;
            } else {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не совпадают</span></div>";
                return false;
            }
        },
        recaptcha: function() {
            var id = Recaptcha.get_response(), is = this.on.recaptcha().nextElementSibling;
            if (id === "") {
                return false;
            }
            if (id.length < 3) {
                is.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не указан код подтверждения</span></div>";
                return false;
            } else {
                is.innerHTML = "";
                return true;
            }
        },
        contract: function() {
            var data = this.on.contract();
            var id = data[0], is = data[1];
            if (!id.checked) {
                is.innerHTML = "<div class='rred sred'><div><div></div></div><span>Пользовательское соглашение</span></div>";
                return false;
            } else {
                is.innerHTML = "";
                return true;
            }
        },
        lock: function(mode) {
            if (this.block === true) {
                this.on.password().setAttribute("type", "text");
                this.on.lock().style.background = "url(/img/icon/offpassico.png) no-repeat";
                document.getElementsByClassName("rtext")[2].style.display = "none";
                this.on.repeatPassword().style.display = "none";
                this.on.repeatPassword().parentNode.nextSibling.style.display = "none";
                this.block = false;
            } else {
                this.on.password().setAttribute("type", "password");
                this.on.lock().style.background = "url(/img/icon/passico.png) no-repeat";
                document.getElementsByClassName("rtext")[2].style.display = "block";
                this.on.repeatPassword().style.display = "block";
                this.on.repeatPassword().parentNode.nextSibling.style.display = "block";
                this.block = true;
                this.repeatPassword();
            }
            if (mode !== undefined) {
                this.check();
            }
        },
        send: function() {
        },
        cfg: {},
        constructor: function(cfg) {
            if (cfg !== undefined && typeof cfg === 'object') {
                if (cfg.on !== undefined) {
                    this.on = cfg.on;
                }
                if (cfg.send !== undefined && typeof cfg.send === 'function') {
                    this.send = cfg.send;
                }
                if (cfg.login !== undefined) {
                    this.cfg.login = cfg.login;
                }
                if (cfg.password !== undefined) {
                    this.cfg.password = cfg.password;
                }
                if (cfg.lock !== undefined && typeof cfg.lock === 'boolean') {
                    this.block = cfg.lock;
                    this.lock();
                }
                this.cfg = cfg;
            }
            this.bind();
            return this;
        }

    };
    return register.constructor(cfg);
}
function recovery(cfg) {
    var recovery = {
        block: false,
        type: 0,
        on: {},
        events: {},
        access: false,
        bind: function() {
            var self = this, list = ['login', 'password', 'repeatPassword'];
            for (var i in list) {
                var name = list[i];
                this.events[name] = this.on[name]().addEventListener('keyup', function() {
                    self[name]();
                    self.check();
                }, false);
            }
            this.on.recaptcha().addEventListener('DOMNodeInserted', function(e) {
                e.target.addEventListener('keyup', function() {
                    self.recaptcha();
                    self.check();
                }, false);
            }, false);
            this.events['lock'] = this.on.lock().querySelector('div').addEventListener('click', function() {
                self.lock();
                self.check();
            }, false);
            var radio = this.on.radio();
            for (var i = 0; i < radio.length; i++) {
                var doc = radio[i];
//                console.info('bind ', doc);
                this.events['radio' + '_' + i] = doc.addEventListener('change', function() {
//                    console.info(this,this.value);
                    self.mode(this.value);
                    self.check();
                }, false);
            }
            this.events['submit'] = this.on.submit().addEventListener('click', function() {
                if (self.access === true) {
                    self.send();
                }
            }, false);

        },
        unbind: function() {
            this.events.forEach(function(event) {
                document.removeEventListener(event);
            });
        },
        exec: function() {
            if (this.check() === false) {
                return false;
            }
            this.send();
        },
        lock: function() {
            if (this.block === true) {
                this.on.password().setAttribute("type", "text");
                this.on.lock().style.background = "url(/img/icon/offpassico.png) no-repeat";
                document.getElementsByClassName("rtext")[2].style.display = "none";
                this.on.repeatPassword().style.display = "none";
                this.on.repeatPassword().parentNode.nextSibling.style.display = "none";
                this.block = false;
            } else {
                this.on.password().setAttribute("type", "password");
                this.on.lock().style.background = "url(/img/icon/passico.png) no-repeat";
                document.getElementsByClassName("rtext")[2].style.display = "block";
                this.on.repeatPassword().style.display = "block";
                this.on.repeatPassword().parentNode.nextSibling.style.display = "block";
                this.block = true;
            }
            this.check();
        },
        login: function() {
            var id = this.on.login();
            if (id.value === "") {
                return false;
            }
            if (id.value.length < this.cfg.login.min) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не менее " + this.cfg.login.min + " символов</span></div>";
                return false;
            } else if (id.value.length > this.cfg.login.max) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не более " + this.cfg.login.max + " символов</span></div>";
                return false;
            } else {
                if (check.mail(id.value) === false) {
                    id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Некорректный E-mail</span></div>";
                    return false;
                } else {
                    id.parentNode.nextSibling.innerHTML = "<div class='rred sgreen'><div><div></div></div><span>Всё отлично</span></div>";
                    return true;
                }
            }
        },
        password: function() {
            var id = this.on.password();
            if (id.value === "") {
                return false;
            }
            if (id.value.length < this.cfg.password.min) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не менее " + this.cfg.password.min + " символов</span></div>";
                return false;
            }
            if (check.levelPassword(id.value) === 1) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Очень слабый</span></div>";
            } else if (check.levelPassword(id.value) === 2) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Слабый</span></div>";
            } else if (check.levelPassword(id.value) === 3) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred syellow'><div><div></div></div><span>Средний</span></div>";
            } else if (check.levelPassword(id.value) === 4) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sgreen'><div><div></div></div><span>Сильный</span></div>";
            }
            return true;
        },
        repeatPassword: function() {
            var id = this.on.repeatPassword();
            if (this.block === false) {
                return true;
            }
            if (id.value === "") {
                return false;
            }
            var rpass = this.on.password().value;
            if (id.value.length < this.cfg.password.min) {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не менее " + this.cfg.password.min + " символов</span></div>";
                return false;
            }
            if (rpass === id.value && id.value !== "") {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sgreen'><div><div></div></div><span>Отлично</span></div>";
                return true;
            } else {
                id.parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не совпадают</span></div>";
                return false;
            }
        },
        recaptcha: function() {
            var id = Recaptcha.get_response(), is = this.on.recaptcha().nextElementSibling;
            console.info(id);
            if (id === "") {
                return false;
            }
            if (id.length < 3) {
                is.innerHTML = "<div class='rred sred'><div><div></div></div><span>Не указан код подтверждения</span></div>";
                return false;
            } else {
                is.innerHTML = "";
                return true;
            }
        },
        check: function() {
            if (this.login() && this.recaptcha()) {
                if (this.type === 1) {
                    if (this.password() && this.repeatPassword()) {
                        this.on.submit().className = "btmon";
                        this.access = true;
//                        console.info('check type:'+this.type+' access:'+this.access)
                        return true;
                    }
                } else {
                    this.on.submit().className = "btmon";
                    this.access = true;
//                    console.info('check type:'+this.type+' access:'+this.access)
                    return true;
                }
            }
            this.on.submit().className = "btmoff";
            this.access = false;
//            console.info('check type:'+this.type+' access:'+this.access)
            return false;
        },
        mode: function(mode) {
            this.type = parseInt(mode);
            if (this.type === 1) {
                document.getElementById("frmpass").style.display = "block";
            } else {
                document.getElementById("frmpass").style.display = "none";
            }
            this.check();
        },
        send: function() {
        },
        cfg: {},
        constructor: function(cfg) {
            if (cfg !== undefined && typeof cfg === 'object') {
                if (cfg.on !== undefined) {
                    this.on = cfg.on;
                }
                if (cfg.send !== undefined && typeof cfg.send === 'function') {
                    this.send = cfg.send;
                }
                if (cfg.login !== undefined) {
                    this.cfg.login = cfg.login;
                }
                if (cfg.password !== undefined) {
                    this.cfg.password = cfg.password;
                }
                if (cfg.lock !== undefined && typeof cfg.lock === 'boolean') {
                    this.block = cfg.lock;
                }
                if (cfg.mode !== undefined && typeof cfg.mode === 'int') {
                    this.type = cfg.mode;
                }
                this.cfg = cfg;
            }
            this.bind();
            return this;
        }
    };
    return recovery.constructor(cfg);
}
module.exports = {
    login: login,
    register: register,
    recovery: recovery
};