/**
 * пока бля собитае не обробытавается
 * @returns {EventStorage.eventStorage}
 */
function EventStorage() {

    var eventStorage = {
        constructor: function() {
            var self = this;
            var handler = function(event) {
                self.handler(event);
            };
            if (window.addEventListener) {
                window.addEventListener("storage", handler, false);
            } else {
                window.attachEvent("onstorage", handler);
            }
            return this;
        },
        /**
         * 
         * StorageEvent {
         key;          // name of the property set, changed etc.
         oldValue;     // old value of property before change
         newValue;     // new value of property after change
         url;          // url of page that made the change
         storageArea;  // localStorage or sessionStorage,
         }
         */
        handler: function(event) {
            console.info(event);
//            if (event.newValue === null) {
//                this.strorage.remove(event.key);
//            } else {
//                this.strorage.set(event.key, event.newValue);
//            }
        },
        session: {},
        local: {}
    };
    return eventStorage.constructor();
}

function storage(storage) {
    var db = {
        storage: null,
        set: function(name, value) {
            if (typeof value === 'object') {
                value = JSON.stringify(value);
            }
            this.storage.setItem(name, value);
            return this;
        },
        get: function(name) {
            var obj = this.storage.getItem(name);
            try {
                return JSON.parse(obj);
            } catch (e) {
                return obj;
            }
        },
        is: function(name) {
            return (this.storage[name] !== undefined);
        },
        remove: function(name) {
            this.storage.removeItem(name);
            return this;
        },
        clear: function() {
            this.storage.clear();
            return this;
        },
        length: function() {
            return this.storage.length;
        },
        constructor: function(storage) {
            if (storage !== undefined && storage instanceof Storage) {
                this.storage = storage;
            } else {
                this.storage = sessionStorage;
            }
            return this;
        },
        load: function(list) {
            if (typeof list === 'string') {
                list = JSON.parse(list);
            }
            for (var name in list) {
                this.set(name, list[name]);
            }
            return this;
        },
        loaded: function() {
            var result = [];
            for (var name in this.storage) {
                result.push(name);
            }
            return result;
        },
        unload: function(list) {
            for (var name in list) {
                this.remove(name);
            }
            return this;
        },
        toString: function() {
            return JSON.stringify(this.storage);
        },
        toJSON: function() {
            return this.storage;
        }

    };
    return db.constructor(storage);
}
function websql(cfg) {
    var cache = {
        cache: {},
        get: function(key, data) {
            var cache = this.cache[key];
            if (cache === undefined) {
                return;
            }
            if (data === cache['data']) {
                return cache['result'];
            }
        },
        set: function(key, data, result) {
            this.cache[key] = {
                data: data,
                result: result
            };
        }
    };
    var websql = {
        resource: null,
        use: null,
        size: 5242880,
        sql: {},
        cache: cache,
        transactions: {},
        dbc: {},
        constructor: function(cfg) {
            if (cfg !== undefined) {
                if (cfg.dbc !== undefined) {
                    this.dbc = cfg.dbc;
                }
                if (cfg.sql !== undefined) {
                    this.sql = cfg.sql;
                }
            }
            this.resource = window.openDatabase(this.use, "1.0", "", this.size);
        },
        db: function(key) {
            var cfg = this.dbc[key];
            if (cfg === undefined) {
                console.error('not foud dbc[key] key= ', key);
                throw Exception();
            }
            if (cfg['resource'] === undefined) {
                cfg['resource'] = window.openDatabase(key, "1.0", "", cfg.size);
            }
            this.use = key;
            for (var name in cfg) {
                this[name] = cfg[name];
            }
            return this;
        },
        query: function(key, d, multi) {
            var cfg = this.sql[key];
            if (cfg === undefined) {
                return false;
            }
            if (cfg.use !== undefined && this.use !== cfg.use) {
                this.db(cfg.use);
            }
            if (cfg.key !== undefined) {
                this.query(cfg.key);
            }
            var data = {};
            for (var name in cfg.depend) {
                data[name] = this[name];
            }
            var result = this.cache.get(key, data);
            if (result !== undefined) {
                if (cfg.table !== undefined && this.transactions[result.table] === true) {
                    return;
                }
                var sql = result.sql;
            } else {
                var sql = cfg.sql, table = cfg.table, self = this, cache = this.cache.set;
                for (var name in data) {
                    sql = sql.replace('{' + data[name] + '}', this[name]);
                    if (table !== undefined) {
                        table = table.replace('{' + data[name] + '}', this[name]);
                    }
                }
                if (table !== undefined) {
                    this.resource.transaction(function(t) {
                        t.executeSql(sql, [], function() {
                            self.transaction[table] = true;
                            cache(key, data, {sql: sql, table: table});
                        });
                    });
                    return;
                }
            }
            if (result === undefined) {
                this.cache.set(key, data, {sql: sql});
            }
            var transaction;
            if (cfg.read === undefined || cfg.read === false) {
                transaction = this.resource.transaction;
            } else {
                transaction = this.resource.readTransaction;
            }
            if (multi === undefined || multi === false) {
                transaction(function(tx) {
                    tx.executeSql(cfg.sql, d, cfg.success, cfg.error);

                });
            } else {
                transaction(function(tx) {
                    cfg.data.forEach(function(d) {
                        tx.executeSql(cfg.sql, d, cfg.success, cfg.error);
                    });

                });
            }
            return true;

        }
    };
    return websql.constructor(cfg);
}
module.exports = {
    storage: storage,
    websql: websql
};