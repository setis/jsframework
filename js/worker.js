window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder;
window.URL = window.URL || window.webkitURL;

function WorkerCode(Code) {
    return new Worker(window.URL.createObjectURL(new BlobBuilder().append(Code).getBlob()));
}

