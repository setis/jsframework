function page() {
    var ajax = require('ajax');

    var page = {
        index: function() {
            ajax.html({
                url: '/content/index.html',
                on: {
                    Complete: function(data) {
                        document.getElementById("main").innerHTML = data;
                        kernel.link.one("main");
                    }
                },
                cache: true
            });
        },
        auth: function() {
            ajax.html({
                url: '/content/auth.html',
                on: {
                    Complete: function(data) {
                        document.getElementById("main").innerHTML = data;
                        kernel.recaptcha = kernel.loader.exec('recaptcha', {id: 'racapchag'});
                        kernel.link.all();
                        kernel.login = new require('auth').login({
                            login: {
                                min: 5,
                                max: 30
                            },
                            password: {
                                min: 4
                            },
                            send: function() {
                                kernel.loading(true);
                                kernel.network.send({
                                    cmd: "login",
                                    data: {
                                        login: document.getElementById("amail").value,
                                        password: document.getElementById("aepass").value,
                                        response: document.getElementById('recaptcha_response_field').value,
                                        challenge: document.getElementById('recaptcha_challenge_field').value}
                                });
                            },
                            on: {
                                login: function() {
                                    return document.getElementById("amail");
                                },
                                password: function() {
                                    return document.getElementById("aepass");
                                },
                                recaptcha: function() {
                                    return document.getElementById("racapchag");
                                },
                                lock: function() {
                                    return document.getElementById('eshowps');
                                },
                                submit: function() {
                                    return document.getElementById('ebuttn');
                                }
                            }
                        });
                    }
                },
                cache: true
            });
        },
        registration: function() {
            ajax.html({
                url: '/content/registration.html',
                on: {
                    Complete: function(data) {
                        document.getElementById("main").innerHTML = data;
                        kernel.recaptcha = kernel.loader.exec('recaptcha', {
                            id: 'racapchag',
                            on: {
                                Success: function() {
                                    console.info('recaptcha.success');
                                    this.self.reload();
                                    this.document().nextElementSibling.innerHTML = "<div class='rred sgreen'><div><div></div></div><span>успешно</span></div>";
                                    return false;
                                },
                                Error: function(msg) {
                                    console.info('recaptcha.error');
                                    this.document().nextElementSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>" + msg + "</span></div>";
                                }
                            }
                        });
                        kernel.link.all();
                        kernel.register = new require('auth').register({
                            login: {
                                min: 5,
                                max: 30
                            },
                            password: {
                                min: 4
                            },
                            send: function() {
                                kernel.loading(true);
                                kernel.network.send({
                                    cmd: "register",
                                    data: {
                                        login: document.getElementById("remail").value,
                                        password: document.getElementById("rpass").value,
                                        response: document.getElementById('recaptcha_response_field').value,
                                        challenge: document.getElementById('recaptcha_challenge_field').value}
                                });
                            },
                            on: {
                                login: function() {
                                    return document.getElementById("remail");
                                },
                                password: function() {
                                    return document.getElementById("rpass");
                                },
                                recaptcha: function() {
                                    return document.getElementById("racapchag");
                                },
                                repeatPassword: function() {
                                    return document.getElementById("repass");
                                },
                                contract: function() {
                                    return [
                                        document.getElementById("checkboxr"),
                                        document.getElementById("lisenze")
                                    ];
                                },
                                lock: function() {
                                    return document.getElementById('showps');
                                },
                                submit: function() {
                                    return document.getElementById('rbuttn');
                                }

                            }
                        });

                    }
                },
                cache: true
            });
        },
        recovery: function() {
            ajax.html({
                url: '/content/recovery.html',
                on: {
                    Complete: function(data) {
                        document.getElementById("main").innerHTML = data;
                        kernel.recaptcha = kernel.loader.exec('recaptcha', {id: 'racapchag'});
                        kernel.link.all();
                        kernel.recovery = new require('auth').recovery({
                            login: {
                                min: 5,
                                max: 30
                            },
                            password: {
                                min: 4
                            },
                            type: 0,
                            on: {
                                login: function() {
                                    return document.getElementById("amail");
                                },
                                password: function() {
                                    return document.getElementById("rpass");
                                },
                                recaptcha: function() {
                                    return document.getElementById("racapchag");
                                },
                                repeatPassword: function() {
                                    return document.getElementById("repass");
                                },
                                contract: function() {
                                    return [
                                        document.getElementById("checkboxr"),
                                        document.getElementById("lisenze")
                                    ];
                                },
                                lock: function() {
                                    return document.getElementById('showps');
                                },
                                submit: function() {
                                    return document.getElementById('rbuttn');
                                },
                                radio: function() {
                                    return document.getElementById('main').querySelectorAll('div.radioToolbar input');
                                }

                            },
                            send: function() {
                                kernel.loading(true);
                                kernel.network.send({
                                    cmd: "recovery",
                                    data: {
                                        login: document.getElementById("amail").value,
                                        response: document.getElementById('recaptcha_response_field').value,
                                        challenge: document.getElementById('recaptcha_challenge_field').value
                                    }
                                });
                            }
                        });
                    }
                },
                cache: true
            });
        },
        rules: function() {
            ajax.html({
                url: '/content/rules.html',
                on: {
                    Complete: function(data) {
                        document.getElementById("main").innerHTML = data;
                        kernel.link.one("main");
                    }
                },
                cache:true
            });
        },
        help: function() {
            ajax.html({
                url: '/content/help.html',
                on: {
                    Complete: function(data) {
                        document.getElementById("main").innerHTML = data;
                        kernel.link.one("main");
                    }
                },
                cache:true
            });
        },
        agreement: function() {
            ajax.html({
                url: '/content/agreement.html',
                on: {
                    Complete: function(data) {
                        document.getElementById("main").innerHTML = data;
                        kernel.link.one("main");
                    }
                },
                cache:true
            });
        },
        test:function(){
            ajax.html({
                url: '/content/message.html',
                on: {
                    Complete: function(data) {
                        document.getElementById("main").innerHTML = data;
                        kernel.link.one("main");
                    }
                }
            });
        }
    };
    return page;
}
module.exports = page;
var page = {
    exit: function() {
        loading('/api/exit.php', 0, null, function(id) {
            console.info(id);
            var array = JSON.parse(id);
            core.cookie.remove_cookie("id");
            redirect("/");
        });
    },
    allcompany: function() {
        controller.send({"cmd": "company_select"});
    },
    profil: function(id) {
        loading('/content/profil.html', 0, null, function(idt) {
            document.getElementById("main").innerHTML = idt;
            loading('/json/user/' + id + '.json', 0, null, function(id) {
                templates.profil(JSON.parse(id));
                core.event.exec('href');
            });
        });
    },
    company: function() {
        loading('/content/company.html', 0, null, function(id) {
            document.getElementById("main").innerHTML = id;
            core.event.exec('href');
        });
    },
    text: function(id, callback) {
        loading('/content/' + id + '.html', 0, null, function(id) {
            document.getElementById("main").innerHTML = id;
            core.event.exec('href');
            if (callback)
                callback();
        });
    },
    addcompany: function() {
        document.getElementById("cmpName").onkeydown = core.litter.limit;
        document.getElementById("cmpInf").onkeydown = core.litter.limit;
        document.getElementById("cmpSite").onkeydown = core.litter.limit;
        //document.getElementById("cmpStats").onkeydown = core.litter.limit;
        document.getElementById("cpmYousite").onkeydown = core.litter.limit;
        document.getElementById("cmpGod").onkeydown = core.litter.number;
    },
    audio: function() {
        var i = 0, arp = document.getElementsByClassName("pla");
        for (; i < arp.length; i++) {
            arp[i].onclick = function() {
                core.audio.play(this);
            };
        }
    },
    video: function() {
        var i = 0, arp = document.getElementsByClassName("plv");
        for (; i < arp.length; i++) {
            arp[i].onclick = function() {
                core.video.play(this);
            };
        }
        var j = 0, arvo = document.getElementsByClassName("vof");
        for (; j < arvo.length; j++) {
            arvo[j].onclick = function() {
                core.video.voloff(this);
            };
        }
        var k = 0, arvlm = document.getElementsByClassName("vlm");
        for (; k < arvlm.length; k++) {
            arvlm[k].onclick = function(e) {
                core.video.volume(this, e);
            };
        }
        var l = 0, arplb = document.getElementsByClassName("plb");
        for (; l < arplb.length; l++) {
            arplb[l].onclick = function(e) {
                core.video.progressClick(this, e);
            };
        }
        var m = 0, arfls = document.getElementsByClassName("fsc");
        for (; m < arfls.length; m++) {
            arfls[m].onclick = function() {
                core.video.fullScreenClick(this);
            };
        }
    }

};