function loader(events,cfg){
	var config = require('functions').cfg;
	var Event = require('event').event;
	var loader = {
		cfg:{},
		events:{},
		constructor:function(events,cfg){
			if(cfg !== undefined && typeof cfg === 'object'){
				this.cfg = cfg;
			}
			this.events = new Event(events,{event:'funcApply'});
			// console.info('loader.init this',this.events.events,this.cfg);
			// console.info('loader.init input',events,cfg);
			return this;
		},
		exec:function(event,cfg){
//			console.info('loader '+event,this.events.events[event]);
			return this.events.event(event,config(this.cfg[event],cfg));
		}
	};
	return loader.constructor(events,cfg);	
}
module.exports = loader;