var
        kernel = {},
        db = require('db'),
        route = require('route'),
        link = require('link'),
        menu = require('menu'),
        network = require('network'),
        controller = require('controller'),
        func = require('func'),
        functions = require('functions'),
        find = require('find'),
        ajax = require('ajax');
//        var Recaptcha = require('recaptcha_ajax');

window.kernel = kernel;
kernel.loading = func.loading;
kernel.redirect = functions.redirect;
//window.$ = require('jquery');
kernel.translates = require('lang');

kernel.pages = new require('page')();
/*
 * action 
 * 1 - читать
 * 2 - описание
 * 3 - скачать
 * 4 - сомментарии
 */
kernel.route = new route.route({
    find: {
        pattern: /^\/(ru|en|jp|ch)\/(find|поиск)\/(.*?)$/gim,
        handler: function (match) {
            if (match.length < 2) {
                return false;
            }
            this.action.apply(this, [match[1], match[3]]);
            return true;
        },
        action: function (lang, values) {
            var list = {};
            switch (lang) {
                case 'ru':
                    list = {found: "not_found", genre: "genre_not"};
                    break;

            }

            var d = find.exclude(list, find.convert(values));
            console.info('asdasda', r, d);
        }
    },
    manga_read: {// manga/ru/манга/$action:$manga_id:$tom:$charaper:$page/онлайн-читать/алиса-в-стране/переводчи-уробос/том-1/глава-1/страница-1/
        pattern: /^\/(ru|en|jp|ch)\/(manga|манга)\/(.*?)\/онлайн-читать\/(.*?)\/перевод-(\d*)-(.*?)\/том-(\d*)\/глава-(\d*)\/страница-(\d*)\/(.*?)$/gim,
        handler: function (match) {
            if (match.length < 2) {
                return false;
            }
            this.action.apply(this, [match[1], match[3]]);
            return true;
        },
        action: function (lang, values) {
            //
            
            console.info('manga');
        }
    }
});
kernel.page = new route.page(kernel.route.rules);
kernel.page.load(
        new require('page')(),
        {
            registration: 'регистрация',
            recovery: 'восстановление',
            auth: ['авторизация', '承認'],
            rules: 'правила',
            help: 'помощь',
            agreement: 'соглашение'
        },
'page');
function linkHandler(id) {
//            console.info('linkHandler 1');
    window.history.pushState('', '', id.getAttribute('href'));
//            console.info('linkHandler 2');
    kernel.route.exec();
//            console.info('linkHandler 3');
    return false;
}
kernel.link = new link({
    footer: {
        document: function () {
            return document.getElementById('footer');
        },
        event: 'click',
        handler: linkHandler,
        onwrite: false
    },
    main: {
        document: function () {
            return document.getElementById('main');
        },
        event: 'click',
        handler: linkHandler,
        onwrite: true
    },
    header: {
        document: function () {
            return document.getElementById('header');
        },
        event: 'click',
        handler: linkHandler,
        onwrite: false
    }
});

kernel.session = new db.storage(sessionStorage);
kernel.cache = new db.storage(localStorage);
kernel.route.exec();
window.addEventListener('popstate', function (e) {
    console.info(e);
    kernel.route.exec();
    kernel.link.all();
}, false);
