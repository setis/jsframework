function json(cfg) {
    var config = {
        handler: "json",
        cache: false,
        ajax: true,
        method:"GET",
        urlEncode: true,
        on: {
            Complete: function(data) {
                console.info(data);
            }
        }
    };
    for (var key in cfg) {
        config[key] = cfg[key];
    }
    return new Ajax(config);

}
function html(cfg) {
    var config = {
        handler: "text",
        cache: false,
        ajax: true,
        urlEncode: false,
        on: {
            Complete: function(data) {
                console.info('ajax.html default ', data);
            }
        },
        xhr: {
            withCredentials: true
        }
    };
    for (var key in cfg) {
        config[key] = cfg[key];
    }
    return new Ajax(config);
}
function Ajax(cfg) {
    window.XMLHttpRequest = window.XDomainRequest || window.XMLHttpRequest;
    window.BlobBuilder = window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder;
    window.URL = window.URL || window.webkitURL;
    var event = require('event').event;
    var xhr = new XMLHttpRequest();
    var on = new event(cfg.on,{event:'funcApply'});
    function urlEncode(obj) {
        var str = [];
        for (var p in obj) {
            str.push(p + "=" + obj[p]);
        }
        return encodeURIComponent(str.join("&"));
    }
    var data = null;
    if (cfg.data !== undefined) {
        data = cfg.data;
    }
    /**
     * 
     *   Событие Описание
     *   loadstart Создается при запуске запроса.
     *   progress Создается с определяемым сервером интервалом, когда запрос отправляет или получает данные.
     *   abort Создается, когда запрос отменяется, например если вызывается метод abort().
     *   error Создается, если запрос завершается ошибкой.
     *   load Создается, когда запрос завершается успешно.
     *   timeout Создается после истечения заданного автором интервала времени.
     *   loadend Создается, когда запрос завершается (успешно или неуспешно).
     */
    var EventListener = [
        'progress', 'load', 'error', 'abort', 'loadend', 'loadstart', 'timeout'
    ];
    var Events = {
        error: function(e) {
            // console.info(e);
            on.event('Error', e);
        },
        loadstart: function(e) {
            // console.info(e);
            on.event('Start', e);
        },
        loadend: function(e) {
            // console.info(e);
            on.event('End', e);
        }
    };
    for (var e in Events) {
        xhr.addEventListener(e, Events[e], false);
    }
    if (typeof cfg.event === 'object') {
        for (var i in EventListener) {
            var event = EventListener[i];
            var handler = cfg.event[event];
            if (typeof handler === 'function') {
                xhr.addEventListener(event, handler, false);
            }
        }
    }
//    var onEventList = ["Start", "Stop", "Complete", "Error", "Success", "Send"];
    for (var e in cfg.xhr) {
        xhr[e] = cfg.xhr[e];
    }
    if (cfg.method === undefined) {
        cfg.method = 'GET';
    } else {
        var methods = [
            'GET',
            'POST'
        ];
        if (methods.some(function(e) {
            return (cfg.method === e);
        }) === false) {
            console.info("not found method: " + cfg.method);
        }
    }
    console.info('ajax.cache',cfg.url,cfg.method,cfg.cache === true ,cfg.method === 'GET' ,(cfg.data === undefined || cfg.data === null));
    if(cfg.cache === true && cfg.method === 'GET' && (cfg.data === undefined || cfg.data === null)){
            var cache = new require('db').storage(localStorage);
            console.info('ajax.cache.is',cache.is(cfg.url));
            if(cache.is(cfg.url)){
                var result = cache.get(cfg.url);
                setTimeout(function(){
                    on.event('Complete', result);
                },5);
                return;
            }else{
                xhr.addEventListener('load',function(e){
                    var result = e.target.response;
                    cache.set(cfg.url,result);
                    on.event('Complete', result);
                });
            }
        }
    


    if (xhr.responseType !== undefined && xhr.responseType) {
        var responseType = [
            'text', 'arraybuffer', 'blob', 'document', 'json'
        ];
        if (responseType.some(function(e) {
            return (xhr.responseType === e);
        }) === false) {
            console.info('not found responseType: ' + xhr.responseType);
        }

    }
    
    if (cfg.timeout !== undefined) {
        xhr.timeout = cfg.timeout;
    }

    
    if (typeof data === 'object') {
        if (data instanceof HTMLFormElement) {
            var form = new FormData(data);
            if (cfg.url === undefined || cfg.url === "" || cfg.url === null) {
                cfg.url = form.action;
            }
            if (data === null) {
                data = form;
            } else {
                for (var name in data) {
                    form.append(name, data[name]);
                }
                data = form;
            }
        } else if (data instanceof FormData) {
            if (cfg.url === undefined || cfg.url === "" || cfg.url === null) {
                cfg.url = data.action;
            }
        } else {
            if (cfg.urlEncode === true) {
                data = urlEncode(data);
            }
        }
    }

    xhr.open(cfg.method, cfg.url, true, cfg.login, cfg.password);
    Response(cfg.handler, xhr, on);
    if (cfg.cache === false) {
        xhr.setRequestHeader("Cache-Control", "no-cache");
        xhr.setRequestHeader("Pragma", "no-cache");
    }
    if (cfg.header !== undefined && cfg.length > 0) {
        var headers = cfg.header;
        for (var header in headers) {
            xhr.setRequestHeader(header, headers[header]);
        }
    }
    
    if (cfg.cfg === true) {
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    }
    xhr.send(data);
    return xhr;
}
function Response(event, xhr, on) {
    var Response = {
        imageUrl: function(xhr, on) {
            xhr.responseType = 'blob';
            xhr.onreadystatechange = function() {
                if (this.readyState === this.DONE && this.status === 200) {
                    var tag = document.createElement('img');
                    tag.addEventListener("load", function(evt) {
                        window.URL.revokeObjectURL(evt.target.src);
                    });
                    tag.src = window.URL.createObjectURL(this.response);
                    on.event('Complete', document.createDocumentFragment().appendChild(tag));
                }
            };
        },
        text: function(xhr, on) {
            try{
            xhr.responseType = 'text';
        }catch(e){
            console.info(e);
        }
            xhr.onreadystatechange = function() {
                if (this.readyState === this.DONE && this.status === 200) {
                    on.event('Complete', this.response);
                }
            };
        },
        json: function(xhr, on) {
            xhr.responseType = 'json';
            xhr.onreadystatechange = function() {
                if (this.readyState === this.DONE && this.status === 200) {
                    on.event('Complete', this.response);
                }
            };
        },
        html: function(xhr, on) {
            try {
                xhr.responseType = 'document';
            } catch (e) {
            }
            xhr.onreadystatechange = function() {
                if (this.readyState === this.DONE && this.status === 200) {
                    // console.info('response',this.response);
                    on.event('Complete', this.response);
                }
            };

        },
        blob: function(xhr, on) {
            xhr.responseType = 'blob';
            xhr.onreadystatechange = function() {
                if (this.readyState === this.DONE && this.status === 200) {
                    on.event('Complete', this.response);
                }
            };
        },
        videoStream: function(xhr, on) {
            xhr.responseType = 'blob';
            xhr.onreadystatechange = function() {
                if (this.readyState === this.LOADING) {
                    var tag = document.createElement("video");
                    tag.addEventListener("loadeddata", function(evt) {
                        window.URL.revokeObjectURL(evt.target.src);
                    });
                    tag.src = window.URL.createObjectURL(this.reponse);
                    on.event('Complete', document.createDocumentFragment().appendChild(tag));
                }
            };
        },
        commet: function(xhr, on) {
            xhr.responseType = 'json';
            xhr.onreadystatechange = function() {
                if (this.readyState >= this.LOADING && this.status === 200) {
                    on.event('Complete', this.response);
                }
            };
        }
    };
    if (Response[event] !== undefined) {
        return Response[event](xhr, on);
    }
    return Response.json(xhr, on);

}
module.exports = {
    ajax: Ajax,
    html: html,
    json: json
};