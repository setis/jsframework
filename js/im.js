function im() {
    var
            send = kernel.network.send,
            user = sessionStorage['user'],
            websql = require('db').wesql,
            cfg = {
                sql: {
                    "dialogs-msg-create": {
                        sql: 'CREATE  TABLE  IF NOT EXISTS "dialogs_msg_{dialog}" ("id" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "user" INTEGER NOT NULL , "msg" BLOB NOT NULL , "time" DATETIME NOT NULL  DEFAULT CURRENT_TIMESTAMP )',
                        depend: ['dialog'],
                        table: "dialogs_msg_{dialog}",
                        use: 'im'
                    },
                    "dialogs-msg-insert": {
                        sql: 'INSERT INTO "dialogs_msg_{dialog}" ("id","user","msg","time") VALUES (?,?,?,?)',
                        depend: ['dialog'],
                        key: "dialogs-dialog-create",
                        use: 'im'
                    },
                    "dialoga-msg-select": {
                        sql: ''
                    }
                },
                dbc: {
                    im: {
                        size: 5242880
                    }
                }
            };

    var db = new websql(cfg);
    var data = {
        user: user,
        dialog: null,
        offset: 0,
        limit: 10,
        input:false
    };
    var im = {
        data:data,
        dialogs: function() {
            send('dialogs');
        },
        dialog: function(dialog) {
            this.data.dialog = dialog;
            send('dialog', dialog);
            this.query('dialogs-msg-select');
        },
        msg: function(msg) {
            send('msg', msg);
        },
        messages: function() {
            send('messages', {offset: this.data.offset, limit: this.data.limit});
        },
        "delete": function(id) {
            send('delete', id);
        },
        input: function(mode) {
            this.data.input = mode;
            send('input',mode);
        }


    };

    var controller = {
        messages: function(data) {
            this.offset += this.limit;

        },
        messages_error: function() {
        },
        dialogs: function(data) {
            this.query('dialogs-dialog-insert', data, true);
        },
        "delete":function(data){
            this.query('dialogs-dialog-delete',data);
        },
        "new":function(data){
            this.query('dialogs-news');
        },
        read:function(data){
            
        }
    };

    im.query = db.query;
    var c = kernel.controller;
    for (var name in controller) {
        c[name] = function(data) {
            return controller.call(im, data);
        };
    }
}
module.exports = im;