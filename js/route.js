function Route(rules) {
    var route = {
        rules: {},
        load: function(rules) {
            for (var name in rules) {
                var rule = rules[name];
                if (rule !== undefined && rule.pattern instanceof RegExp
                        && (rule.handler !== undefined && typeof rule.handler === 'function')) {
                    this.rules[name] = rule;
                } else {
                    console.info("failed rule: " + name, rule);
                }
            }
            return this;
        },
        loaded: function() {
            var result = [];
            for (var name in this.rules) {
                result.push(name);
            }
            return result;
        },
        unload: function(names) {
            names.forEach(function(name) {
                delete this.rules[name];
            });
            return this;
        },
        add: function(name, pattern, handler, action) {
            if (typeof name !== 'string') {
                return false;
            }
            if (!pattern instanceof RegExp) {
                return false;
            }

            if (typeof handler !== 'function') {
                return false;
            }
            if (action !== undefined && typeof action !== 'function') {
                action = undefined;
            }
            this.rules[name] = {
                action: action,
                handler: handler,
                pattern: pattern
            };
            return this;

        },
        remove: function(name) {
            if (this.rules[name] !== undefined) {
                delete this.rules[name];
            }
            return this;
        },
        exec: function() {
            var url = decodeURIComponent(window.location.pathname);
            for (var rule in this.rules) {
                var obj = this.rules[rule];
                var result = obj.pattern.exec(url);
                //bug fixed
                obj.pattern.exec(url);
//                console.info('route.rules.' + rule, this.rules[rule], url, result);
                if (result !== null && result.length > 0) {
                    result = obj.handler(result);
//                    console.info('route.rules.' + rule, result);
                    if (result !== false) {
                        console.info('route.rules.' + rule, result);
                        return result;
                    } else {
                        console.info('not route.rules.' + rule, result);
                    }
                }
            }
        },
        toString: function() {
            return 'route';
        }

    };
    if (rules !== undefined) {
        route.load(rules);
    }
    return route;
}
function PageRoute(rules) {
    var pageRoute = {
        routes: {},
        rules: {},
        events: {},
        generate: function(array) {
//            console.info(array);
            var list = '(';
            for (var el in array) {
                var rule = array[el];
                if (typeof rule === 'string') {
                    list += array[el] + '|';
                } else {
                    for (var r in rule) {
                        list += rule[r] + '|';
                    }
                }
            }
            for (var el in array) {
                list += el + '|';
            }
            return list.substr(0, list.length - 1) + ')';
        },
        pattern: function(pattern) {
//            console.info(pattern);
            return new RegExp('^\\S' + pattern + '?$', 'igm');
        },
        action: function(event, section) {

            var routes = this.routes[section];
//            console.info(event, section, routes);
            if (routes[event] !== undefined) {
                console.info('found event ' + event, section);
                this.event(event, section);
                return true;
            } else {
                for (var name in routes) {
                    var route = routes[name];
//                    console.info('route ', route);
                    if (typeof route === 'string') {
                        if (route === event) {
                            console.info('found event ' + name, section);
                            this.event(name, section);
                            return true;
                        }
                    } else {
//                        console.info(name, route);
                        for (var i = 0; i <= route.length; i++) {
//                            console.info(event, route[i]);
                            if (route[i] === event) {
                                console.info('found event ' + name, event, section);
                                this.event(name, section);
                                return true;
                            }
                        }
                    }
                }
                console.info(' not found event ' + event + ' section: ' + section);
                return false;
            }
        },
        load: function(events, routes, section) {
//            console.info(events,section,routes);
            this.routes[section] = routes;
            var self = this;
//            console.info(this.rules);
            this.rules[section] = {
                pattern: this.pattern(this.generate(routes)),
                handler: function(match) {
//                    console.info(self.action, match[1], section, self.action(match[1], section));
                    return self.action(match[1], section);
                }
            };
//            console.info(this.rules[section]);
            this.events.select(section);
            this.events.load(events);
            this.events.save();
//            console.info('route.load ', this.events.events, this.events._events, this.events.use);
            return this;
        },
        event: function(event, section) {
//            console.info('route.event', this.events.events, this.events._events, this.events.use,section);
            this.events.select(section);
            // console.info('route.event', this.events.events,event,this.events.events[event], this.events._events, this.events.use);
            this.events.event(event);
            return this;

        },
        constructor: function(rules) {
            var Event = require('event').event;
            this.events = new Event({}, {
                select: {
                    enable: true
                }
            });
            if (rules !== undefined && typeof rules === 'object') {
                this.rules = rules;
            }
            return this;
        }
    };
    return pageRoute.constructor(rules);
}
module.exports = {
    route: Route,
    page: PageRoute
};