"use strict";
function EventSimplex(events) {
    var eventSimplex = {
        events: {},
        cfg: {},
        register: function(event) {
            if (this.cfg.history !== undefined && this.cfg.history === true) {
                this.history.handler(event, true);
            }
        },
        unregister: function(event) {
            if (this.cfg.history !== undefined && this.cfg.history === true) {
                this.history.handler(event, false);
            }
        },
        event: function(event) {
            // console.info(event, this.events[event]);
            if (this.events[event] !== undefined) {
                var handler = this.events[event];
                this.register(event);
                // console.info(this._event);
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                return this._event(handler, args);
            } else {
                this.unregister(event);
            }
        },
        load: function(events) {
           // console.info('event.load', events);
            if (this.events === undefined) {
                this.events = {};
            }
            for (var event in events) {
                var handler = events[event];
                if (typeof handler === 'function') {
                    this.events[event] = handler;
                }
            }
            return this;
        },
        unload: function(events) {
            for (var event in events) {
                if (this.events[event] !== undefined) {
                    delete(this.events[event]);
                }
            }
            return this;
        },
        loaded: function() {
            var result = [];
            for (var event in this.events) {
                result.push(event);
            }
            return result;
        },
        add: function(event, handler) {
            if (typeof handler === 'function') {
                this.events[event] = handler;
            }
            return this;
        },
        remove: function(event) {
            if (this.events[event] !== undefined) {
                delete(this.events[event]);
            }
            return this;
        },
        reset: function() {
            this.events = {};
            return this;
        },
        toString: function() {
            return 'EventSimplex';
        },
        constructor: function(events) {
            if (events !== undefined && typeof events === 'object') {
                this.load(events);
            }
            return this;
        }

    };
    return eventSimplex.constructor(events);

}
function EventDispatch(events) {
    var event = {
        events: {},
        cfg: {},
        register: function(event) {
            if (this.cfg.history !== undefined && this.cfg.history === true) {
                this.history.handler(event, true);
            }
        },
        unregister: function(event) {
            if (this.cfg.history !== undefined && this.cfg.history === true) {
                this.history.handler(event, false);
            }
        },
        event: function(event) {
            if (this.events[event] !== undefined) {
                var handler = this.events[event];
                this.register(event);
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                switch (typeof handler) {
                    case 'function':
                        if (typeof handler === 'function') {
                            return handler.apply(this, args);
                        }
                        break;
                    case 'object':
                        handler.forEach(function(func) {
                            func.apply(this, args);
                        });
                        break;
                    default:
                        console.error('not func in event: ' + event, handler);
                        break;
                }
            } else {
                this.unregister(event);
            }
            return this;
        },
        load: function(events) {
            for (var event in events) {
                var handler = events[event];
                switch (typeof handler) {
                    case 'function':
                        this.events[event] = handler;
                        break;
                    case 'object':
                        this.events[event] = handler.filter(function(func) {
                            return (typeof func === 'function');
                        });
                        break;
                }
            }
            return this;
        },
        unload: function(events) {
            for (var event in events) {
                if (this.events[event] !== undefined) {
                    delete(this.events[event]);
                }
            }
            return this;
        },
        loaded: function() {
            var result = [];
            for (var event in this.events) {
                result.push(event);
            }
            return result;
        },
        add: function(event, handler) {
            var events = this.events[event];
            if (events === undefined) {
                switch (typeof handler) {
                    case 'function':
                        this.events[event] = handler;
                        break;
                    case 'object':
                        this.events[event] = handler.filter(function(func) {
                            return (typeof func === 'function');
                        });
                        break;
                }
            } else if (events instanceof Array) {
                switch (typeof handler) {
                    case 'function':
                        this.events[event].push(handler);
                        break;
                    case 'object':
                        this.events[event].concat(handler.filter(function(func) {
                            return (typeof func === 'function');
                        }));
                        break;
                }
            }
            return this;
        },
        remove: function(event) {
            if (this.events[event] !== undefined) {
                delete(this.events[event]);
            }
            return this;
        },
        reset: function() {
            this.events = {};
            return this;
        },
        toString: function() {
            return 'EventeDispatcher';
        },
        constructor: function(events) {
            if (events !== undefined && typeof events === 'object') {
                this.load(events);
            }
            return this;
        }

    };
    return event.constructor(events);
}
function EventSelect() {
    var eventSelect = {
        use: null,
        _events: {},
        events: {},
        constructor: function(events, select) {
            if (events === undefined && typeof events !== 'object') {
                throw Error();
            } else {
                if (events instanceof EventSimplex) {
                    this._events = events.events;
                } else {
                    this._events = events;
                }
            }

            if (select === undefined) {
                this.use = null;
            } else {
                if (this.select(select)) {
                    return false;
                }
            }
            return this;
        },
        select: function(select) {
            var events = this._events;
//            console.info("event.select ",select, this.use,this._events,this.events );
            if (typeof select !== 'object') {
                select = new Array(select);
            }
            if (this.use !== null && this.use.toString() === select.toString()) {
                // console.info('event.select use === select');
                return this;
            }
            select.forEach(function(select) {
//                console.info('event.select.foreach pre ',events,select);
                events = events[select];
//                console.info('event.select.foreach ',events);
                if (events === undefined) {
                    events = {};
                }
            });
            this.events = events;
            this.use = select;
//            console.info('use ', this.use);
//            console.info('events ', events);
            return this;
        },
        save: function() {
            var events = this._events;
            this.use.forEach(function(select) {
                if (events[select] === undefined) {
                    events[select] = {};
                }
                events = events[select];
            });
            for (var name in this.events) {
//                console.info('event.select.save ',name,this.events[name]);
                events[name] = this.events[name];
            }
//            console.info('event.select.save ',this._events);
            return this;
        },
        restrict: function() {
            this._events = this.events;
            this.use = null;
            return this;
        }

    };
    return eventSelect;
}
function EventHistory() {
    var eventHistory = {
        history: [],
        events: {},
        limit: 0,
        handler: function(event, register) {
            this.history.unshift(event);
            if (this.limit > 0 && this.history.length >= this.limit) {
                this.history.pop();
            }
            this.events[event] = register;
        },
        reset: function() {
            this.history = [];
        }

    };
    return eventHistory;
}
function EventCollector() {
    var eventCollector = {
        events: [],
        create: function() {
            var id = this.events.push({});
            return this.events[--id];
        },
        remove: function(id) {
            if (id !== undefined) {
                throw Error('not input id');
            }
            if (this.events[id] !== undefined) {
                delete this.events[id];
            }
            return this;
        },
        add: function(events) {
            if (events === undefined && typeof events !== 'object' && events.events === undefined) {
                throw Error();
            }
            var id = this.events.push(Object.create(events.events));
            events.events = this.events[--id];
            return this;

        }
    };
    return eventCollector;
}
function Event(events, cfg) {
    if (cfg === undefined) {
        return new EventSimplex(events);
    }
    var event;
    if (cfg.dispatch === true) {
        event = new EventDispatch();
    } else {
        event = new EventSimplex();
        cfg.dispatch = false;
        if (cfg.event !== undefined && typeof cfg.event === 'function') {
            event._event = cfg.event;
        } else {
            switch (cfg.event) {
                case 'funcApplyObj':
                case 1:
                    event._event = function(handler, args) {
                        if (typeof handler === 'function') {
                            // console.info(args);
                            var self = args.shift();
                            return handler.apply(self, args);
                        }
                    };
                    break;
                case 'funcApply':
                case 2:
                    event._event = function(handler, args) {
                        if (typeof handler === 'function') {
                            // console.info(args);
                            return handler.apply(this, args);
                        }
                    };
                    break;
                default:
                case 'func':
                case 0:
                    event._event = function(handler) {
                        if (typeof handler === 'function') {
                            return handler();
                        }
                    };
                    break;
            }
        }

        if (cfg.event !== undefined) {
            delete cfg.event;
        }
        // console.info('event._event ', event._event);
    }


    event.cfg = cfg;
    if (cfg.collector !== undefined && cfg.collector.enable !== undefined && cfg.collector.enable === true) {
//        event.cfg.collector.enable === true;
        if (cfg.collector.bind !== undefined && cfg.collector.bind instanceof EventCollector) {
            event.events = cfg.collector.bind.create();
        } else {
            if (window.Events === undefined) {
                window.Events = new EventCollector();
            }
            event.events = window.Events.create();
        }
    }
    event.load(events);
    if (cfg.select !== undefined && cfg.select.enable !== undefined && cfg.select.enable === true) {
//        event.cfg.collector.enable === true;
        var obj = new EventSelect();
        for (var key in obj) {
            var value = obj[key];
            event[key] = value;
//            console.info(key,value);
        }
//        event.cfg.select.enable = cfg.select.enable;
//        console.info(event.select,cfg.select.use,EventSelect);

        if (cfg.select.use !== undefined && cfg.select.use instanceof Array) {
            event._events = event.events;
            event.select(cfg.select.use);
//            console.info(event.events, event._events, cfg.select.use);
        }

        if (cfg.select.restrict !== undefined && cfg.select.restrict === true) {
//            console.info(event.events, event._events);
            event.restrict();
        }

    }
    if (cfg.history !== undefined && cfg.history.enable !== undefined && cfg.history.enable === true) {
        if (cfg.history.bind !== undefined && cfg.history.bind instanceof EventHistory) {
            event.history = cfg.history.bind;
        } else {
            event.history = new EventHistory();
        }
        if (cfg.history.limit !== undefined && cfg.history.limit) {
            event.history.limit = cfg.history.limit;
        }
    }

    return event;
}
module.exports = {
    simplex: EventSimplex,
    dispatch: EventDispatch,
    collector: EventCollector,
    event: Event
};
//var cfg = {
//    history: {
//        enable: true,
//        limit: 0,
//        bind: {}
//    },
//    select: {
//        enable: true,
//        restrict: true,
//        use: ['list', 'user']
//    },
//    collector: {
//        enable: true,
//        bind: {}
//    }
//};
//var events = {
//    list: {
//        user: {
//            alex: null,
//            marine: null
//        }
//    }
//};
//var e = new Event(events, cfg);
//console.info(e);