var functions = require('functions');
function convert(values) {
    var r = functions.parseQuery(values);
    var result = {};
    for (var i in r) {
        var value = r[i].split(',');
        var arr = [];
        for (var s in value) {
            var d = parseInt(value[s]);
            if (!isNaN(d)) {
                arr.push(d);
            }
        }
        if (arr.length !== 0) {
            result[i] = arr;
        }

    }
    return result;
}

function exclude(list, values) {
    for (var name1 in list) {
        var f1, f2 = true;
        var name2 = list[name1];
        if (values[name1] !== undefined) {
            console.info(values[name1],functions.arr_diff(values[name1], values[name1]));
            values[name1] = functions.arr_diff(values[name1], values[name1]);
        } else {
            f1 = false;
        }
        if (values[name2] !== undefined) {
            console.info(values[name2],functions.arr_diff(values[name2], values[name2]));
            values[name2] = functions.arr_diff(values[name2], values[name2]);
        } else {
            f2 = false;
        }
        if (f1 !== false && f2 !== false) {
            console.info(functions.exclusion([1,2,3], [1,2,3,4]));
            values[name2] = functions.exclusion(values[name1], values[name2]);
        }

    }
    return values;
}

module.exports = {
    convert: convert,
    exclude: exclude
};