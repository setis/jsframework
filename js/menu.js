function Menu(cfg) {
    var menu = {
        exec: function() {
            var cache = false;
            if (this.onCache !== undefined && this.onCache === true) {
                cache = true;
            }
            var active = this.path.current();
            var items = this.root.getChilders(active);
            var html = '';
            var handlers = {};
            if (cache === true && this.cache.is(active) === true) {
                return this.cache.read(active);
            }
            var item = this.items.get(active);
            var self = this;
//            console.info('menu.exec ', active, items);
            html += this.active(active, item.name, item.href, item.handler);
            items.forEach(function(obj,item){
                if (obj !== undefined) {
                    html += self.template(item, obj.name, obj.href, obj.handler);
                    if (obj.handler !== undefined && typeof obj.handler === 'function') {
                        handlers[item] = function() {
                            obj.handler(item);
                        };
                    }
                }
            });
            if (cache === true) {
                this.cache.write(active, html, handlers);
            }
            return {
                document: html,
                handlers: handlers
            };
        },
        handlers: function(handlers) {
            for (var handler in handlers) {
                handler();
            }
        },
        load: function(menu) {
            for (var i in menu) {
                var obj = menu[i];
//                console.info('menu.load ', obj.root, obj.childers);
                this.root.add(obj.root, obj.childers);
            }
            return this;
        },
        template: function(id, title, href, handler) {
        },
        active: function(id, title, href, handler) {
        },
        make: function(document) {

        },
        build: function() {
            var obj = this.exec();
//            console.info('menu.build',obj);
            this.make(obj.document);
            this.handlers(obj.handlers);
        },
        constructor: function(cfg) {
            if (cfg !== undefined && cfg instanceof Object) {
                var list = [
                    'template',
                    'active',
                    'make'
                ];
                var self = this;
                list.forEach(function(name) {
                    var tmp = cfg[name];
                    if (tmp !== undefined && typeof tmp === 'function') {
                        self[name] = tmp;
                    }
                });
                if (cfg.list !== undefined && cfg.list instanceof Array) {
                    this.load(cfg.list);
                }
                if (cfg.cache !== undefined && typeof cfg.cache === 'boolean') {
                    this.onCache === cfg.cache;
                } else {
                    this.onCache === false;
                }
            }
            return this;
        }


    };
    var cache = {
        cache: {},
        write: function(active, html, handlers) {
            this.cache[active] = {
                document: html,
                handlers: handlers
            };
        },
        read: function(active) {
            return this.cache[active];
        },
        is: function(active) {
            return (this.cache[active] !== undefined);
        }
    };
    var items = {
        items: [],
        add: function(item) {
//            console.info('menu.items.add',item,(item instanceof Array));
            if (item instanceof Array) {
                var result = [];
                for (var i in item) {
                    result.push(this.items.push(item[i]) - 1);
                }
//                console.info('menu.items.add->result', result, this.items);
                return result;
            }
            return this.items.push(item) - 1;
        },
        remove: function(item) {
            if (item instanceof Array) {
                for (var i in item) {
                    var obj = item[i];
                    if (this.items[obj] !== undefined) {
                        delete this.items[obj];
                    }
                }
            } else {
                if (this.items[item] !== undefined) {
                    delete this.items[item];
                }
            }
            return this;
        },
        get: function(item) {
            if (item instanceof Array) {
                var result = [];
                var items = this.items;
                item.forEach(function(item){
                    result[item] = items[item];
                });
//                console.info('menu.items.get',result);
                return result;
            } else {
                if (this.items[item] !== undefined) {
                    return this.items[item];
                }
            }
            return false;
        }
    };
    var root = {
        childers: {},
        items: {},
        rootList: function() {
            var result = [];
            for (var root in this.childers) {
                result.push(root);
            }
            return result;
        },
        childerList: function() {
            var result = [];
            for (var root in this.childers) {
                result.concat(this.childers[root]);
            }
            return result;
        },
        addRoot: function(item) {
            var root = this.items.add(item);
            this.childers[root] = [];
            return root;
        },
        addChilders: function(root, childers) {
            var itemChilders = this.items.add(childers);
            if (this.childers[root] === undefined) {
                if (itemChilders instanceof Array) {
                    this.childers[root] = itemChilders;
                }else{
                    this.childers[root] = new Array(itemChilders);
                }
            } else {
                this.childers[root] = this.childers[root].concat(itemChilders);
            }
//            console.info('menu.addChilders', root, childers, itemChilders, this.childers[root]);
            return this;

        },
        add: function(root, childers) {
            return this.addChilders(this.addRoot(root), childers);
        },
        getChilders: function(root) {
            if (this.childers[root] === undefined) {
                return [];
            }
            var childers = this.childers[root];
            
            var items = this.items.get(childers);
//            console.info('menu.root.getChilders', childers, items);
            return items;
        },
        getRoot: function(childer) {
            for (var root in this.childers) {
                var childers = this.childers[root];
                for (var i in childers) {
                    if (childer === childers[i]) {
                        return root;
                    }
                }
            }
        },
        removeChilder: function(childer, root) {
            if (root === undefined) {
                for (var root in this.childers) {
                    var childers = this.childers[root];
                    for (var i in childers) {
                        if (childer === childers[i]) {
                            this.items.remove(childers[i]);
                            delete this.childers[root][i];
                        }
                    }
                }
            }
            var childers = this.childers[root];
            for (var i in childers) {
                if (childer === childers[i]) {
                    this.items.remove(childers[i]);
                    delete this.childers[root][i];
                }
            }
            return this;

        },
        removeRoot: function(root) {
            if (this.childers[root] === undefined) {
                return false;
            }
            this.items.remove(this.childers[root]);
            delete this.childers[root];
            return this;
        }
    };
    var path = {
        path: [0],
        select: null,
        current: function() {
            if (this.select === undefined || this.select === null) {
                if (this.path.length === 0) {

                    return null;
                }
                this.select = this.path[this.path.length--];
            }
            return this.select;
        },
        next: function(length) {
            var select = this.current();
            var count = (length === undefined) ? 1 : length;
            for (var i = 0; i <= this.path.length; i++) {
                var path = this.path[i];
                if (select === path) {
                    count--;
                }
                if (count === 0) {
                    break;
                }
            }
            return path;
        },
        prev: function(length) {
            var select = this.current();
            var count = (length === undefined) ? 1 : length;
            for (var i = this.path.length; i >= 0; i--) {
                var path = this.path[i];
                if (select === path) {
                    count--;
                }
                if (count === 0) {
                    break;
                }
            }
            return path;
        },
        constructor: function(select) {
            if (select !== undefined && typeof select === 'int') {
                this.select = select;
                this.path.push(select);
            } else {
                this.select = 0;
                this.path.push(0);
            }
            return this;
        }
    };
    menu.cache = cache;
    menu.items = items;
    menu.root = root;
    menu.root.items = items;
    menu.childers = root.childers;
    menu.path = path.constructor(0);
    return menu.constructor(cfg);
}
module.exports = Menu;

