function select(db, select, onSuccess, onError) {
    if (db === undefined && select === undefined) {
        if (onError !== undefined && typeof onError === 'function') {
            onError();
        }
        return false;
    }
    var use = db;
    switch (typeof select) {
        case 'object':
            for (var s in select) {
//                console.info(use[select[s]],select[s]);
                use = use[select[s]];
                if (use === undefined) {
                    if (onError !== undefined && typeof onError === 'function') {
                        onError();
                    }
                    return false;
                }
            }
            break;
        default:
            use = use[select];
//            console.info(use[select]);
            if (use === undefined) {
                if (onError !== undefined && typeof onError === 'function') {
                    onError();
                }
                return false;
            }
            break;

    }
    if (onSuccess !== undefined && typeof onSuccess === 'function') {
        onSuccess(use, select, db);
    }
    return use;
}
function size(x, mb) {
    var table = [
        {
            byte: 1073741824,
            name: "Gb"
        },
        {
            byte: 1048576,
            name: "Mb"
        },
        {
            byte: 1024,
            name: "Kb"
        }

    ];
    for (var i = 0; i <= table.length; i++) {
        var size = table[i];
        if (x >= size.byte) {
            return (Math.floor(x / size.byte)) + ' ' + size.name;
        }
    }
    return (x) + ' B';
}
function create() {
    var create = {
        css: function (cfg) {
            var tag = document.createElement('link');
            tag.rel = 'stylesheet';
            tag.href = window.URL.createObjectURL(new Blob([cfg.source], {type: 'text/css'}));
            if (cfg.Success !== undefined && typeof cfg.Success === ' function') {
                tag.addEventListener("load", function (e) {
                    var frag = document.createDocumentFragment().appendChild(tag);
                    if (cfg.Document !== undefined && typeof cfg.Document === 'function') {
                        cfg.Document(frag);
                    } else {
                        var head = document.head || document.getElementsByTagName('head')[0];
                        head.appendChild(frag);
                    }
                    return cfg.Success(e);
                });
            }
            if (cfg.Error !== undefined && typeof cfg.Error === ' function') {
                tag.addEventListener("error", cfg.Error);
            }
        },
        js: function (cfg) {
            var tag = document.createElement('script');
            tag.type = 'text/javascript';
            tag.charset = (cfg.charset === undefined) ? 'utf8' : cfg.charset;
            tag.async = (cfg.async === undefined || cfg.async === true) ? true : false;
            if (cfg.Success !== undefined && typeof cfg.Success === ' function') {
                tag.addEventListener("load", function (e) {
                    var frag = document.createDocumentFragment().appendChild(tag);
                    if (cfg.Document !== undefined && typeof cfg.Document === 'function') {
                        cfg.Document(frag);
                    } else {
                        var head = document.head || document.getElementsByTagName('head')[0];
                        head.appendChild(frag);
                    }
                    return cfg.Success(e);
                });
            }
            if (cfg.Error !== undefined && typeof cfg.Error === ' function') {
                tag.addEventListener("error", cfg.Error);
            }
            tag.src = window.URL.createObjectURL(new Blob([cfg.source], {type: 'text/javascript'}));
        },
        image: function (cfg) {
            var tag = document.createElement('image');
            if (cfg.Success !== undefined && typeof cfg.Success === ' function') {
                tag.addEventListener("load", function (e) {
                    var frag = document.createDocumentFragment().appendChild(tag);
                    if (cfg.Document !== undefined && typeof cfg.Document === 'function') {
                        cfg.Document(frag);
                    } else {
                        var head = document.head || document.getElementsByTagName('head')[0];
                        head.appendChild(frag);
                    }
                    return cfg.Success(e);
                });
            }
            if (cfg.Error !== undefined && typeof cfg.Error === ' function') {
                tag.addEventListener("error", cfg.Error);
            }
            tag.src = window.URL.createObjectURL(new Blob([cfg.source], {type: 'image/' + cfg.ext}));
        }
    };
    return create;
}
function screen() {
    var fullScreen = {
        open: function (element) {
            if (element.requestFullScreen) {
                element.requestFullScreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        },
        is: function () {
            return document.fullscreenEnabled || document.mozFullscreenEnabled || document.webkitIsFullScreen ? true : false;
        },
        close: function () {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            }
            else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            }
            else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    };
    return fullScreen;
}
//function initilze() {
//    window.XMLHttpRequest = window.XDomainRequest || window.XMLHttpRequest;
//    window.BlobBuilder = window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder;
//    window.URL = window.URL || window.webkitURL;
//    document.fullscreenEnabled = document.fullscreenEnabled || document.mozFullscreenEnabled || document.webkitIsFullScreen;
//    document.cancelFullScreen = document.cancelFullScreen || document.mozCancelFullScreen || document.webkitCancelFullScreen;
//    var fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
//    var fullscreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled;
//}
function check() {
    var check = {
        mail: function (value) {
            var n = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;
            return n.test(value);

        },
        levelPassword: function (value) {
            var c = 0;
            var l = new Array(1, 2, 3, 4, 5);
            var lvl = 0;
            if (value.length < 5) {
                c = (c + 7);
            } else if (value.length > 4 && value.length < 8) {
                c = (c + 14);
            } else if (value.length > 7 && value.length < 16) {
                c = (c + 17);
            } else if (value.length > 15) {
                c = (c + 23);
            }
            if (value.match(/[a-z]/)) {
                c = (c + 9);
            }
            if (value.match(/[A-Z]/)) {
                c = (c + 10);
            }
            if (value.match(/\d+/)) {
                c = (c + 10);
            }
            if (value.match(/(.*[0-9].*[0-9].*[0-9])/)) {
                c = (c + 10);
            }
            if (value.match(/.[!,@,#,$,%,^,&,*,?,_,~]/)) {
                c = (c + 10);
            }
            if (value.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/)) {
                c = (c + 10);
            }
            if (value.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                c = (c + 7);
            }
            if (value.match(/([a-zA-Z])/) && value.match(/([0-9])/)) {
                c = (c + 7);
            }
            if (value.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/)) {
                c = (c + 15);
            }
            if (c < 21) {
                lvl = 1;
            } else if (c > 20 && c < 30) {
                lvl = 2;
            } else if (c > 29 && c < 43) {
                lvl = 3;
            } else if (c > 42 && c < 60) {
                lvl = 4;
            } else {
                lvl = 5;
            }
            return lvl;
        }
    };
    return check;
}
function ligthBox(cfg) {
    function stopEvent(evt) {
        (evt && evt.stopPropagation) ? evt.stopPropagation() : window.event.cancelBubble = true;
    }
    var ligthBox = {
        pageYOffset: 0,
        mode: false,
        event: null,
        block: false,
        cfg: {
            lock: false,
            event: true,
            once: true

        },
        document: function () {
            return document.getElementById("lbx");
        },
        content: function () {
            return document.getElementById("cont");
        },
        constructor: function (cfg) {
            if (cfg !== undefined && cfg instanceof Object) {
                if (cfg.event !== undefined && typeof cfg.event === 'boolean') {
                    this.cfg.event = cfg.event;
                }
                if (cfg.lock !== undefined && typeof cfg.lock === 'boolean') {
                    this.cfg.lock = cfg.lock;
                }
                if (cfg.document !== undefined && typeof cfg.document === 'function') {
                    this.document = cfg.document;
                }
                if (cfg.content !== undefined && typeof cfg.content === 'function') {
                    this.content = cfg.content;
                }
                if (cfg.once !== undefined && typeof cfg.once === 'boolean') {
                    this.cfg.once = cfg.once;
                }
            }
            if (this.cfg.event === true && this.cfg.once === true) {
                var self = this;
                this.event = this.document().addEventListener('click', function (event) {
                    self.hide();
                    stopEvent(event);
                }, true);
            }
            return this;
        },
        html: function (html) {
            this.content().innerHTML = html;
            return this;
        },
        lock: function (mode) {
            if (mode === undefined || typeof mode !== 'boolean') {
                this.block = (this.block === true) ? false : true;
            } else {
                this.block = mode;
            }
            return this;
        },
        show: function (html) {
            if (html !== undefined) {
                this.content().innerHTML = html;
            }
            this.pageYOffset = window.pageYOffset;
            document.body.style.overflowY = "hidden";
            document.body.style.position = "fixed";
            document.body.style.marginTop = "-" + this.pageYOffset + "px";
            var doc = this.document();
            doc.style.display = "block";
            if (this.cfg.event === true && this.cfg.once === false) {
                var self = this;
                this.event = doc.addEventListener('click', function (event) {
                    self.hide();
                    stopEvent(event);
                }, true);
            }

            this.mode = true;
            this.block = true;
            return true;
        },
        hide: function () {
            if (this.cfg.lock === true && this.block === true) {
                return false;
            }
            document.body.style.overflowY = "auto";
            document.body.style.position = "relative";
            document.body.style.marginTop = "0";
            window.scrollBy(0, this.pageYOffset);
            this.document().style.display = "none";
            this.mode = false;
            if (this.cfg.event === true && this.cfg.once === false) {
                document.removeEventListener(this.event);
            }
            return true;
        }

    };
    return ligthBox.constructor(cfg);
}
function redirect(url) {
    history.pushState('', '', url);
    kernel.route.exec();
    return false;
}
function cfg(defaults, current) {
    var config = defaults;
    if (config === undefined) {
        return current;
    }
    // console.info(current);
    for (var key in current) {
        config[key] = current[key];
    }
    return config;
}

function parseQuery(query) {
    var arr = {};
    var q = query.split('&');
    for (var s in q) {
        var el = q[s].split('=');
        arr[el[0]] = el[1];
    }
    return arr;
}
function exclusion(a1, a2) {    // убираем повторения в off массиве, относительно массива on
    var k = 0, moff = a2.length, mon = a1.length, narr = [];
    for (; k < moff; k++) {
        var l = 0, add = true;
        for (; l < mon; l++) {
            if (a2[k] === a1[l]) {
                add = false;
                break;
            }
        }
        if (add) {
            narr.push(a2[k]);
        }
    }
    return narr;
}
function arr_diff(a1, a2)
{
    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {
                delete a[a2[i]];
            } else {
                a[a2[i]] = true;
            }
        }
    }
    for (var k in a) {
        diff.push(k);
    }
    return diff;
}
module.exports = {
    select: select,
    size: size,
    create: create,
    screen: screen,
    check: check,
    cfg: cfg,
    ligthBox: ligthBox,
    redirect: redirect,
    parseQuery: parseQuery,
    exclusion: exclusion,
    arr_diff: arr_diff
};