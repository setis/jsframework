function ws(cfg) {

    var EventDispatch = require('event').dispatch;
    var ws = {
        traffic: null,
        reconnect: null,
        keepalive: null,
        cfg: {
            debug: false,
            url: null,
            reconnect: true,
            traffic: false,
            keepalive: true
        },
        on: {},
        controller: {},
        ws: null,
        get connected() {
            if (this.ws === null) {
                return false;
            }
            return(this.ws.readyState === 1 || this.ws.readyState === 0) ? true : false;
        },
        set connected(status) {
//            console.info('connected ' + status, this.cfg.reconnect);
            if (this.cfg.reconnect === true) {
                if (status === false) {
                    this.reconnect.handler(false);
                }
            }
        },
        translate: function(code) {
            return code;
        },
        initilize: function() {
            var self = this;
            this.ws.onopen = function() {
                self.open.call(self);
            };
            this.ws.onclose = function(event) {
                self.close.call(self, event);
            };
            this.ws.onmessage = function(event) {
                self.message.call(self, event);
            };
            this.ws.onerror = function(event) {
                self.error.call(self, event);
            };
        },
        connect: function(url) {
            if (url !== undefined) {
                this.cfg.url = url;
            }
            if (this.cfg.reconnect === true) {
                // console.info(this.reconnect);
                this.reconnect.mode = true;
            }
            this.ws = new WebSocket(this.cfg.url);
            this.initilize();

        },
        disconnect: function() {
            if (this.cfg.reconnect === true) {
                this.reconnect.mode = false;
            }
            this.ws.close();
        },
        open: function(event) {
            this.connected = true;
            if (this.cfg.debug) {
                console.info(this.translate('connect'));
            }
            this.on.event('open', event);

        },
        close: function(event) {
            if (this.cfg.debug) {
                if (event.wasClean) {
                    console.info(this.translate('closeClean'));
                } else {
                    console.info(this.translate('close')); // например, "убит" процесс сервера
                }
//                console.info(this.translate('code') + ': ' + event.code + this.translate('reason') + ' : ' + event.reason);
            }
            this.connected = false;
            this.on.event('close', event);
        },
        message: function(event) {
            if (event.data === '') {
                return;
            }
            if (this.cfg.debug) {
                console.info(this.translate('message') + " " + event.data);
            }
            if (this.cfg.traffic.enable === true) {
                this.traffic.recv(event.data);
            }
            if (this.cfg.keepalive === true) {
                this.keepalive.handler(true);
            }
            this.handler(event.data);
            this.on.event('message', event);

        },
        error: function(error) {
            if (this.cfg.debug) {
                console.info(this.translate('error') + error);
            }
            this.connected = false;
            this.on.event('error', error);
        },
        send: function(data) {
            if (typeof data !== 'string') {
                data = JSON.stringify(data);
            }
            this.ws.send(data);
            if (this.cfg.traffic.enable === true) {
                this.traffic.send(data);
            }
            if (this.cfg.debug) {
                console.info(this.translate('send') + data);
            }

        },
        handler: function(d) {
            try {
                var data = JSON.parse(d);
            }
            catch (e) {
                console.info(this.translate('error_json_decode') + "  " + e.message);
                console.info(d);
                return false;
            }
            try {
                if (this.controller[data.cmd] === undefined) {
                    if (this.debug) {
                        console.info(this.translate('not_found_controller'), data);
                    }
                    return false;
                } else {
                    this.controller[data.cmd](data.data, data.time);
                    if (this.debug) {
                        console.info(this.translate('found_controller'), this.controller[data.cmd]);
                    }
                    return true;
                }
            } catch (e) {
                console.info(this.translate('error controller') + ' ' + data.cmd + ' ' + e);
            }
        },
        constructor: function(cfg) {
            this.on = new EventDispatch();
            if (cfg !== undefined) {
                if (cfg.reconnect !== undefined) {
                    var config = cfg.reconnect;
                    this.reconnect = new reconnect(config);
                    this.cfg.reconnect = (config.enable !== undefined && typeof config.enable === 'boolean') ? config.enable : this.cfg.reconnect;
//                    this.cfg.reconnect = config;
                }
                if (cfg.keepalive !== undefined) {
                    var config = cfg.keepalive;
                    this.keepalive = new keepalive(config);
//                    console.info(typeof config.enable);
                    this.cfg.keepalive = (config.enable !== undefined && typeof config.enable === 'boolean') ? config.enable : this.cfg.keepalive;
                }
                if (cfg.traffic !== undefined) {
                    this.traffic = new traffic();
                    this.cfg.traffic = (config.traffic !== undefined && typeof config.traffic === 'boolean') ? config.traffic : this.cfg.traffic;
//                    this.cfg.traffic = cfg.traffic;
                }
                if (cfg.controller !== undefined && typeof cfg.controller === 'object') {
                    this.controller = cfg.controller;
                }
                if (cfg.url !== undefined) {
                    this.cfg.url = cfg.url;
                }
                if (cfg.debug !== undefined && typeof cfg.debug === 'boolean') {
                    this.cfg.debug = cfg.debug;
                }
            }
            this.connect();
            return this;
        }
    };
    return ws.constructor(cfg);
}
function sse(cfg) {
    var EventDispatch = require('event').dispatch;
    var sse = {
        traffic: null,
        reconnect: null,
        cfg: {
            debug: false,
            url: null,
            reconnect: {
                enable: false,
                timeout: 15000,
                action: function() {
                    sse.connect();
                }

            },
            traffic: {
                enable: false
            }

        },
        on: {},
        controller: {},
        es: null,
        get connected() {
            if (this.es === null) {
                return false;
            }
            return(this.es.readyState === 1 || this.es.readyState === 0) ? true : false;
        },
        set connected(status) {
            console.info('connected ' + status);
            if (status === false) {
                if (this.reconnect.enable === true) {
                    this.reconnect.handler();
                }
            }
        },
        translate: function(code) {
            return code;
        },
        initilize: function() {
            var self = this;
            this.es.onopen = function() {
                self.open.call(self);
            };
            this.es.onclose = function(event) {
                self.close.call(self, event);
            };
            this.es.onmessage = function(event) {
                self.message.call(self, event);
            };
            this.es.onerror = function(event) {
                self.error.call(self, event);
            };
        },
        connect: function(url) {
            if (url !== undefined) {
                this.cfg.url = url;
            }
            if (this.cfg.reconnect.enable === true) {
                this.reconnect.mode = true;
            }
            this.es = new EventSource(this.cfg.url);
            this.initilize();

        },
        disconnect: function() {
            if (this.cfg.reconnect.enable === true) {
                this.reconnect.mode = false;
            }
            this.es.close();
        },
        open: function() {
            this.connected = true;
            if (this.cfg.debug) {
                console.info(this.translate('connect'));
            }
            this.on.event('open');

        },
        close: function(event) {
            if (this.cfg.debug) {
                if (event.wasClean) {
                    console.info(this.translate('closeClean'));
                } else {
                    console.info(this.translate('close')); // например, "убит" процесс сервера
                }
                console.info(this.translate('code') + ': ' + event.code + this.translate('reason') + ' : ' + event.reason);
            }
            this.connected = false;
            this.on.event('close');
        },
        message: function(event) {
            if (event.data === '') {
                return;
            }
            if (this.cfg.debug) {
                console.info(this.translate('message') + " " + event.data);
            }
            if (this.cfg.traffic.enable === true) {
                this.traffic.recv(event.data);
            }
            this.reconnect.handler(true);
            this.handler(event.data);
            this.on.event('message');

        },
        error: function(error) {
            if (this.cfg.debug) {
                console.info(this.translate('error') + error.message);
            }
            this.connected = false;
            this.on.event('error');
        },
        handler: function(d) {
            try {
                var data = JSON.parse(d);
            }
            catch (e) {
                console.info(this.translate('error_json_decode') + "  " + e.message);
                console.info(d);

                return false;
            }
            if (this.cfg.debug) {
                console.info(data);
            }
            try {
                if (this.controller[data.cmd] === undefined) {
                    if (this.debug) {
                        console.info(data);
                    }
                    return false;
                } else {
                    this.controller[data.cmd](data.data, data.time);
                    return true;
                }
            } catch (e) {
                console.info(this.translate('not_found_controller') + ' ' + data.cmd + ' ' + e);
            }
        },
        constructor: function(cfg) {
            this.on = new EventDispatch();
            if (cfg !== undefined) {
                if (cfg.reconnect !== undefined) {
                    var config = cfg.reconnect;
                    this.reconnect = new reconnect(config.connectd, config.action, config.timeout);
                    this.cfg.reconnect = config;
                }
                if (cfg.traffic !== undefined) {
                    this.traffic = new traffic();
                    this.cfg.traffic = cfg.traffic;
                }
                if (cfg.controller !== undefined && typeof cfg.controller === 'object') {
                    this.controller = cfg.controller;
                }
                if (cfg.url !== undefined) {
                    this.cfg.url = cfg.url;
                }
                if (cfg.debug !== undefined && typeof cfg.debug === 'boolean') {
                    this.cfg.debug = cfg.debug;
                }
            }
            return this;
        }
    };
    return sse.constructor(cfg);
}
function traffic() {
    var traffic = {
        sentBytes: 0,
        recvBytes: 0,
        table: [
            {
                byte: 1073741824,
                name: "Gb"
            },
            {
                byte: 1048576,
                name: "Mb"
            },
            {
                byte: 1024,
                name: "Kb"
            }

        ],
        send: function(data) {
            this.sentBytes += data.length;
        },
        recv: function(data) {
            this.recvBytes += data.length;
        },
        size: function(x) {
            for (var i = 0; i <= this.table.length; i++) {
                var size = this.table[i];
                if (x >= size.byte) {
                    return (Math.floor(x / size.byte)) + ' ' + size.name;
                }
            }
            return (x) + ' B';
        },
        status: function() {
            console.info(this.translate('recv') + ': ' + this.size(this.recvBytes));
            console.info(this.translate('send') + ': ' + this.size(this.sentBytes));
        },
        reset: function() {
            this.sentBytes = 0;
            this.recvBytes = 0;
            return this;
        }

    };
    return traffic.constructor();
}
function keepalive(cfg) {
    var keepalive = {
        timeout: 15000,
        event: null,
        action: null,
        handler: function(status) {
            var self = this;
//            console.info('keepalive', this, status, this.event, this.connected);
            if (status === true) {
                clearTimeout(this.event);
                this.event = setTimeout(function() {
                    self.handler.call(self, false);
                }, this.timeout);
            } else {
                this.action();
                clearTimeout(this.event);
                this.event = setTimeout(function() {
                    self.handler.call(self, false);
                }, this.timeout);
            }
        },
        constructor: function(cfg) {
            if (cfg !== undefined) {
                if (cfg.timeout !== undefined && typeof cfg.timeout === 'number') {
                    this.timeout = cfg.timeout;
                }
                if (cfg.action !== undefined && typeof cfg.action === 'function') {
                    this.action = cfg.action;
                }
            }
//            console.info('keepalive', this.action, this.timeout);
            return this;
        }

    };
    return keepalive.constructor(cfg);
}
function reconnect(cfg) {
    var reconnect = {
        timeout: 0,
        event: null,
        connected: null,
        action: null,
        mode: false,
        handler: function() {
            if (this.mode === true && this.connected() === false) {
                if (this.event === null) {
                    var self = this;
                    this.event = setInterval(function() {
                        if (self.mode === true && self.connected() === false) {
                            self.action();
                        } else {
                            clearInterval(self.event);
                            self.event = null;
                        }
                    }, this.timeout);
                }
            }
        },
        constructor: function(cfg) {
            if (cfg !== undefined) {
                if (cfg.timeout !== undefined && typeof cfg.timeout === 'number') {
                    this.timeout = cfg.timeout;
                }
                if (cfg.connected !== undefined && typeof cfg.connected === 'function') {
                    this.connected = cfg.connected;
                }
                if (cfg.action !== undefined && typeof cfg.action === 'function') {
                    this.action = cfg.action;
                }
            }
//            console.info('reconnect', this.connected, this.action, this.timeout);
            return this;
        }

    };
    return reconnect.constructor(cfg);
}
function load() {
    var load = {
        css: function(cfg) {
            var tag = document.createElement('link');
            tag.type = 'text/css';
            tag.rel = "stylesheet";
            if (cfg.Success !== undefined && typeof cfg.Success === ' function') {
                tag.addEventListener("load", function(e) {
                    var frag = document.createDocumentFragment().appendChild(tag);
                    if (cfg.Document !== undefined && typeof cfg.Document === 'function') {
                        cfg.Document(frag);
                    } else {
                        var head = document.head || document.getElementsByTagName('head')[0];
                        head.appendChild(frag);
                    }
                    return cfg.Success(e);
                });
            }
            if (cfg.Error !== undefined && typeof cfg.Error === ' function') {
                tag.addEventListener("error", cfg.Error);
            }
            tag.src = cfg.url;
            return tag;
        },
        js: function(cfg) {
            var tag = document.createElement('script'); //Создаёте новый тег <SCRIPT> 
            tag.type = 'text/javascript';
            tag.charset = (cfg.charset === undefined) ? 'utf8' : cfg.charset;
            tag.async = (cfg.async === undefined || cfg.async === true) ? true : false;
            if (cfg.Success !== undefined && typeof cfg.Success === ' function') {
                tag.addEventListener("load", function(e) {
                    var frag = document.createDocumentFragment().appendChild(tag);
                    if (cfg.Document !== undefined && typeof cfg.Document === 'function') {
                        cfg.Document(frag);
                    } else {
                        var head = document.head || document.getElementsByTagName('head')[0];
                        head.appendChild(frag);
                    }
                    return cfg.Success(e);
                });
            }
            if (cfg.Error !== undefined && typeof cfg.Error === 'function') {
                tag.addEventListener('error', cfg.Error);
            }
            tag.src = cfg.url;
            return tag;
        },
        image: function(cfg) {
            var tag = document.createElement('img');
            if (cfg.Success !== undefined && typeof cfg.Success === ' function') {
                tag.addEventListener("load", function(e) {
                    var frag = document.createDocumentFragment().appendChild(tag);
                    if (cfg.Document !== undefined && typeof cfg.Document === 'function') {
                        cfg.Document(frag);
                    }
                    return cfg.Success(e);
                });
            }
            if (cfg.Error !== undefined && typeof cfg.Error === 'function') {
                tag.addEventListener('error', cfg.Error);
            }
            tag.src = cfg.url;
            return tag;

        },
        video: function(cfg) {
            var tag = document.createElement('video');
            if (cfg.Success !== undefined && typeof cfg.Success === ' function') {
                tag.addEventListener("load", function(e) {
                    var frag = document.createDocumentFragment().appendChild(tag);
                    if (cfg.Document !== undefined && typeof cfg.Document === 'function') {
                        cfg.Document(frag);
                    }
                    return cfg.Success(e);
                });
            }
            if (cfg.Error !== undefined && typeof cfg.Error === 'function') {
                tag.addEventListener('error', cfg.Error);
            }
            tag.src = cfg.url;
            return tag;
        }
    };
    return load;
}
module.exports = {
    ws: ws,
    sse: sse,
    load: load,
    keepalive: keepalive,
    reconnect: reconnect
};