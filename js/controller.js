function Controller(kernel) {
    if (kernel === undefined) {
        kernel = window.kernel;
    }
    var controller = {
        evel: function(data) {
            eval(data);
        },
        setCookie: function(cookies) {
            cookies.forEach(function(val) {
                $.cookie(val.name, val.value, {
                    expires: val.expires,
                    path: val.path,
                    domain: val.domain,
                    secure: val.secure
                });
            });
        },
        removeCookie: function(cookies) {
            cookies.forEach(function(name) {
                $.cookie(name, null);
            });
        },
        getCookie: function() {
            kernel.network.send(document.cookie);
        },
        restoreSession: function(data) {
            kernel.session.load(data);
        },
        getSession: function() {
            kernel.network.send(kernel.session.storage);
        },
        setSession: function(data) {
            kernel.session.load(data);
        },
        recaptcha: function() {
            kernel.recaptcha.success();
        },
        recaptcha_error: function(data) {
            kernel.loading(false);
            kernel.recaptcha.error(data);
        },
        login: function(user) {
            kernel.loading(false);
//            kernel.session.set('user',user);
            localStorage['user'] = user;
            kernel.redirect('/my');
        },
        login_error:function(data){
            kernel.loading(false);
            console.info(data);
        },
        register_error: function(data) {
            kernel.loading(false);
            console.info(data);
            var errors = [
                'service_off',
                'email_not_input',
                'email_not_validate',
                'email_busy',
                'password_not_input',
                'password_not_min',
                'password_not_max'
            ],
                    login = [1, 2, 3],
                    password = [4, 5, 6],
                    translate = new require('translation').plugin(kernel.translates, 'register'),
                    error = errors[data.code],
                    msg;
            if (error !== undefined) {
                msg = translate.translate(error);
            }
            else {
                msg = translate.translate(errors[0]);
            }

            var box = login.indexOf(data.code);
            if (box > 0) {
                box = 'login';
            } else {
                box = password.indexOf(data.code);
                if (box > 0) {
                    box = 'password';
                }
            }
            if (box !== -1) {
                kernel.register.on[box]().parentNode.nextSibling.innerHTML = "<div class='rred sred'><div><div></div></div><span>" + msg + "</span></div>";
            } else {

            }
//            Recaptcha.reload();
        }


    };
    controller.cookie = controller.setCookie.bind(controller);

    return controller;
}
module.exports = Controller;